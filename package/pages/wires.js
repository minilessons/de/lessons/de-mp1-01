function xypoint(x,y) {
  this.x = x;
  this.y = y;
  this.symbol = 0; // 0...bez prikaza; 1...puni kružić; 2...prazan kružić; 3...strelica gornja
  this.relative = function(dx,dy) {
    return new xypoint(this.x+dx, this.y+dy);
  }
  this.updateTo = function(x,y) {
    this.x = x;
    this.y = y;
    return this;
  }
  this.translate = function(dx,dy) {
    this.x += dx;
    this.y += dy;
    return this;
  }
  this.updateX = function(x) {
    this.x = x;
    return this;
  }
  this.updateY = function(y) {
    this.y = y;
    return this;
  }
  this.updateToPoint = function(p) {
    this.x = p.x;
    this.y = p.y;
    return this;
  }
  this.copy = function() {
    return new xypoint(this.x, this.y);
  }
  this.interpolateTo = function(p, t) {
    return new xypoint(
      this.x + t*(p.x-this.x),
      this.y + t*(p.y-this.y)
    );
  }
  this.draw = function(ctx) {
    if(this.symbol == 0) return;
    if(this.symbol == 1) {
      ctx.fillStyle = "#000000";
      ctx.beginPath();
      ctx.arc(this.x, this.y, 3, 0, 2*Math.PI);
      ctx.fill();
    }
    if(this.symbol == 2) {
      ctx.fillStyle = "#ffffff";
      ctx.beginPath();
      ctx.arc(this.x, this.y, 3, 0, 2*Math.PI);
      ctx.fill();
      ctx.fillStyle = "#000000";
      ctx.beginPath();
      ctx.arc(this.x, this.y, 3, 0, 2*Math.PI);
      ctx.stroke();
    }
  }
}

function flowValue() {
  this.value = 0;
}

function wire(flow,start) {
  this.points = [];
  this.points.push(start);
  this.flowValue = flow;
  this.addPoint = function(p) {
    this.points.push(p);
    return this;
  }
  this.setSymbol = function(index, sym) {
    this.points[index].symbol = sym;
    return this;
  }
  this.interpolatePoint = function(index, t) {
    return new xypoint(
      this.points[index].x + t*(this.points[index+1].x-this.points[index].x),
      this.points[index].y + t*(this.points[index+1].y-this.points[index].y)
    );
  }
  this.advance = function(dx, dy) {
    var p = this.points[this.points.length-1];
    this.points.push(new xypoint(p.x+dx, p.y+dy));
    return this;
  }
  this.advanceInterpolateToPoint = function(p, t) {
    let p1 = this.points[this.points.length-1];
    this.points.push(new xypoint(p1.x + t*(p.x-p1.x), p1.y));
    this.points.push(new xypoint(p1.x + t*(p.x-p1.x), p.y));
    this.points.push(p.copy());
    return this;
  }
  this.advanceToX = function(x) {
    var p = this.points[this.points.length-1];
    this.points.push(new xypoint(x, p.y));
    return this;
  }
  this.advanceToY = function(y) {
    var p = this.points[this.points.length-1];
    this.points.push(new xypoint(p.x, y));
    return this;
  }
  this.advanceToPoint = function(p) {
    this.points.push(new xypoint(p.x, p.y));
    return this;
  }
  this.nextXY = function(x,y) {
    this.points.push(new xypoint(x, y));
    return this;
  }
  this.last = function() {
    return this.points[this.points.length-1];
  }
  this.first = function() {
    return this.points[0];
  }
  this.fromEnd = function(i) {
    return this.points[this.points.length-1-i];
  }
  this.fromStart = function(i) {
    return this.points[i];
  }
  this.count = function() {
    return this.points.length;
  }
  this.draw = function(ctx, animTime, deltaTime) {
    var p0 = this.points[0];
    ctx.strokeStyle = "#000000";
    ctx.beginPath();
    ctx.moveTo(p0.x, p0.y);
    for(var index = 1; index < this.points.length; index++) {
      var p1 = this.points[index];
      ctx.lineTo(p1.x, p1.y);
    }
    ctx.stroke();
    if(this.flowValue && this.flowValue.value > 1E-6) {
      ctx.lineWidth = 2;
      ctx.setLineDash([5,10]);
      ctx.lineDashOffset = -((animTime/100) % (5+10));
      p0 = this.points[0];
      ctx.strokeStyle = "#00ff00";
      ctx.beginPath();
      ctx.moveTo(p0.x, p0.y);
      for(var index = 1; index < this.points.length; index++) {
        var p1 = this.points[index];
        ctx.lineTo(p1.x, p1.y);
      }
      ctx.stroke();
      ctx.setLineDash([]);
      ctx.lineDashOffset = 0;
      ctx.lineWidth = 1;
    }
    for(var index = 0; index < this.points.length; index++) {
      var p1 = this.points[index];
      if(p1.symbol != 0) p1.draw(ctx);
    }

  }
}

