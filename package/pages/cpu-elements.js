// Depends on: wires.js
// Imena boja: http://www.rapidtables.com/web/color/RGB_Color.htm

var cpuElements = {};

cpuElements.vgVectorToString = function(arr, zeroIsMsb) {
  let s = [];
  if(zeroIsMsb) {
    for(let i = 0; i < arr.length; i++) {
      s.push(arr[i].getValue());
    }
  } else {
    for(let i = arr.length-1; i >= 0; i--) {
      s.push(arr[i].getValue());
    }
  }
  return s.join("");
}

// topLeft: position of top-left corner
// address: array of value getters
// addressMsbIsFirst: true if address[0] is MSB; false otherwise
// e: value getter
// outputs: array of value getters
// outputsMsbIsFirst: true if outputs[0] is MSB; false otherwise
// Note: value getters are objects with no-argument function getValue() which obtain value of input/output.
cpuElements.binaryDecoder = function(topLeft, address, addressMsbIsFirst, e, outputs, outputsMsbIsFirst) {
  this.topLeft = topLeft.copy();
  this.width = 50;
  let on = Math.pow(2, address.length);
  this.height = Math.max(100, on*25);
  this.addrPoint = new xypoint(this.topLeft.x,this.topLeft.y + this.height/2);
  this.ePoint = new xypoint(this.topLeft.x+this.width/2,this.topLeft.y);
  this.oPoints = [];
  for(let i = 0; i < on; i++) {
    this.oPoints.push(new xypoint(this.topLeft.x+this.width,this.topLeft.y + this.height *(i+1)/(on+1)));
  }
  this.bgColor = "#ADFF2F";  // greenyellow

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();
    
    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(cpuElements.vgVectorToString(address, addressMsbIsFirst), this.addrPoint.x-4, this.addrPoint.y-4);
    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("A", this.addrPoint.x+4, this.addrPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    let os = cpuElements.vgVectorToString(outputs, outputsMsbIsFirst);
    for(let i = 0; i < on; i++) {
      ctx.fillText(os.charAt(on-1-i), this.oPoints[i].x+4, this.oPoints[i].y-4);
    }
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    for(let i = 0; i < on; i++) {
      ctx.fillText("y"+i, this.oPoints[i].x-4, this.oPoints[i].y);
    }

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(""+e.getValue(), this.ePoint.x+4, this.ePoint.y-4);

    ctx.textAlign = "center";
    ctx.textBaseline = "top";
    ctx.fillText("E", this.ePoint.x, this.ePoint.y+4);

    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("dekoder", this.topLeft.x+this.width/2, this.topLeft.y+this.height-1);
  }
}

// topLeft: position of top-left corner
// data: array of value getters
// dataMsbIsFirst: true if data[0] is MSB; false otherwise
// ce: value getter
// cp: value getter
// outputs: array of value getters
// outputsMsbIsFirst: true if outputs[0] is MSB; false otherwise
// reset: value getter
// Note: value getters are objects with no-argument function getValue() which obtain value of input/output.
cpuElements.parallelRegister = function(topLeft, data, dataMsbIsFirst, ce, cp, outputs, outputsMsbIsFirst, res) {
  this.topLeft = topLeft.copy();
  this.width = 80;
  this.height = 150;
  this.dataPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/4);
  this.cpPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/4);
  this.cePoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*3/4);
  this.oPoint = new xypoint(this.topLeft.x+this.width, this.topLeft.y+this.height/2);
  this.resPoint = new xypoint(this.topLeft.x+this.width/2, this.topLeft.y);
  this.hideDataValue = false;
  this.bgColor = "#CCFFCC";

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    let cpl = 6;
    ctx.beginPath();
    ctx.moveTo(this.cpPoint.x, this.cpPoint.y-cpl);
    ctx.lineTo(this.cpPoint.x+cpl, this.cpPoint.y);
    ctx.lineTo(this.cpPoint.x, this.cpPoint.y+cpl);
    ctx.stroke();
    
    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    if(!this.hideDataValue) ctx.fillText(cpuElements.vgVectorToString(data, dataMsbIsFirst), this.dataPoint.x-4, this.dataPoint.y-4);
    ctx.fillText(""+cp.getValue(), this.cpPoint.x-4, this.cpPoint.y-4);
    ctx.fillText(""+ce.getValue(), this.cePoint.x-4, this.cePoint.y-4);
    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("DATA", this.dataPoint.x+4, this.dataPoint.y);
    ctx.fillText("CLK", this.cpPoint.x+8, this.cpPoint.y);
    ctx.fillText("CE", this.cePoint.x+4, this.cePoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    let os = cpuElements.vgVectorToString(outputs, outputsMsbIsFirst);
    ctx.fillText(os, this.oPoint.x+4, this.oPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("Q", this.oPoint.x-4, this.oPoint.y);

    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("registar", this.topLeft.x+this.width/2, this.topLeft.y+this.height-1);

    ctx.textAlign = "center";
    ctx.textBaseline = "top";
    ctx.fillText("RESET", this.resPoint.x, this.resPoint.y+4);
    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(""+res.getValue(), this.resPoint.x+4, this.resPoint.y-4);
  }
}

// topLeft: position of top-left corner
// address: array of value getters
// addressMsbIsFirst: true if address[0] is MSB; false otherwise
// inputs: array of arrays of value getters: [vector0, vector1, ..., vectorNm1]
// inputsMsbIsFirst: true if inputs[0] is MSB; false otherwise
// outputs: array of value getters
// outputsMsbIsFirst: true if outputs[0] is MSB; false otherwise
// Note: value getters are objects with no-argument function getValue() which obtain value of input/output.
cpuElements.vectorMux = function(topLeft, address, addressMsbIsFirst, inputs, inputsMsbIsFirst, outputs, outputsMsbIsFirst) {
  this.topLeft = topLeft.copy();
  this.hideInputValues = false;
  this.width = 50;
  let on = Math.pow(2, address.length);
  this.height = Math.max(100, on*50);
  this.rightFactor = 0.75;
  this.addrPoint = new xypoint(this.topLeft.x + this.width/2, this.topLeft.y + this.height*(1-this.rightFactor)/4);
  this.iPoints = [];
  for(let i = 0; i < on; i++) {
    this.iPoints.push(new xypoint(this.topLeft.x, this.topLeft.y + this.height *(i+1)/(on+1)));
  }
  this.oPoint = new xypoint(this.topLeft.x + this.width, this.topLeft.y + this.height/2);
  this.bgColor = "#FFA500"; // narančasta

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.moveTo(this.topLeft.x, this.topLeft.y);
    ctx.lineTo(this.topLeft.x + this.width, this.topLeft.y + this.height*(1-this.rightFactor)/2);
    ctx.lineTo(this.topLeft.x + this.width, this.topLeft.y + this.height*(1-(1-this.rightFactor)/2));
    ctx.lineTo(this.topLeft.x, this.topLeft.y + this.height);
    ctx.closePath();
    ctx.fill();
    ctx.stroke();
    
    ctx.font="12px Georgia";
    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(cpuElements.vgVectorToString(address, addressMsbIsFirst), this.addrPoint.x+4, this.addrPoint.y-4);
    ctx.textAlign = "center";
    ctx.textBaseline = "top";
    ctx.fillText("A", this.addrPoint.x, this.addrPoint.y+4);

    if(!this.hideInputValues) {
      ctx.textAlign = "right";
      ctx.textBaseline = "bottom";
      for(let i = 0; i < on; i++) {
        let os = cpuElements.vgVectorToString(inputs[i], inputsMsbIsFirst);
        ctx.fillText(os, this.iPoints[i].x-4, this.iPoints[i].y-4);
      }
    }

    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    for(let i = 0; i < on; i++) {
      ctx.fillText("d"+i, this.iPoints[i].x+4, this.iPoints[i].y);
    }

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(cpuElements.vgVectorToString(outputs, outputsMsbIsFirst), this.oPoint.x+4, this.oPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "center";
    ctx.fillText("y", this.oPoint.x-4, this.oPoint.y);

    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("mux", this.topLeft.x+this.width/2, this.topLeft.y+this.height-1-this.height*(1-this.rightFactor)/2*0.7);
  }
}

// middleRight: point of right side vertically middle
// valueGetters: array of objects with getValue():v function
// valueSetters: array of objects with setValue(v):void function
// toggleValueProvider: object with nextValue(v):v  and previousValue(v):v functions
// labels: array of labels
cpuElements.vectorSetterBox = function(middleRight, valueGetters, valueSetters, toggleValueProvider, labels, uw, uh) {
  this.middleRight = middleRight.copy();
  this.unitWidth = uw || 30;
  this.unitHeight = uh || 30;
  this.topLeft = new xypoint(middleRight.x - valueGetters.length*this.unitWidth, middleRight.y - this.unitHeight/2);
  this.boxes = [];
  this.labelPlacement = 0; //0-top, 1-left, 2-bottom, 3-right, -1: don't show
  this.caption = null;
  this.captionPlacement = -1;
  this.width = this.unitWidth * valueGetters.length;
  this.backgroundColor = "#DDDDDD";

  function action(i) {
    this.fire = function(info) {
      let v = valueGetters[i].getValue();
      let leftClick = info.button == 0;
      let nv = leftClick ? toggleValueProvider.nextValue(v) : toggleValueProvider.previousValue(v);
      valueSetters[i].setValue(nv);
    }
  }
  for(let i = 0; i < valueGetters.length; i++) { 
    this.boxes.push({x: this.topLeft.x+i*this.unitWidth, y: this.topLeft.y, w: this.unitWidth, h: this.unitHeight, a: new action(i)});
  }
  this.registerActiveRegions = function(activeRegions) {
    for(let i = 0; i < this.boxes.length; i++) {
      let box = this.boxes[i];
      activeRegions.add(new ActiveRegion(
        box.x, box.y, box.w, box.h, {
          execute: function(info) {box.a.fire(info);}
        }
      ));
    }
  }
  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.backgroundColor;
    for(let i = 0; i < valueGetters.length; i++) { 
      ctx.beginPath();
      ctx.rect(this.topLeft.x+i*this.unitWidth, this.topLeft.y, this.unitWidth, this.unitHeight);
      ctx.fill();
      ctx.stroke();
    }

    ctx.fillStyle = "#000000";
    ctx.font="12px Georgia";
    for(let i = 0; i < valueGetters.length; i++) { 
      ctx.textAlign = "center";
      ctx.textBaseline = "middle";
      ctx.fillText(""+valueGetters[i].getValue(), this.topLeft.x+(i+0.5)*this.unitWidth, this.topLeft.y+this.unitHeight/2);
      if(labels && labels[i]) {
        if(this.labelPlacement==0) {
          ctx.textBaseline = "bottom";
          ctx.fillText(labels[i], this.topLeft.x+(i+0.5)*this.unitWidth, this.topLeft.y-4);
        } else if(this.labelPlacement==1) {
          ctx.textAlign = "right";
          ctx.textBaseline = "middle";
          ctx.fillText(labels[i], this.topLeft.x+i*this.unitWidth-4, this.topLeft.y+this.unitHeight/2);
        } else if(this.labelPlacement==2) {
          ctx.textBaseline = "top";
          ctx.fillText(labels[i], this.topLeft.x+(i+0.5)*this.unitWidth, this.topLeft.y+this.unitHeight+4);
        } else if(this.labelPlacement==3) {
          ctx.textAlign = "left";
          ctx.textBaseline = "middle";
          ctx.fillText(labels[i], this.topLeft.x+(i+1)*this.unitWidth+4, this.topLeft.y+this.unitHeight/2);
        }
      }
    }
    if(this.caption != null && this.captionPlacement != -1) {
      ctx.textAlign = "center";
      ctx.textBaseline = "middle";
      if(this.captionPlacement==0) {
        ctx.textBaseline = "bottom";
        ctx.fillText(this.caption, this.topLeft.x+0.5*this.width, this.topLeft.y-4);
      } else if(this.captionPlacement==1) {
        ctx.textAlign = "right";
        ctx.textBaseline = "middle";
        ctx.fillText(this.caption, this.topLeft.x-4, this.topLeft.y+this.unitHeight/2);
      } else if(this.captionPlacement==2) {
        ctx.textBaseline = "top";
        ctx.fillText(this.caption, this.topLeft.x+0.5*this.width, this.topLeft.y+this.unitHeight+4);
      } else if(this.captionPlacement==3) {
        ctx.textAlign = "left";
        ctx.textBaseline = "middle";
        ctx.fillText(this.caption, this.topLeft.x+this.width+4, this.topLeft.y+this.unitHeight/2);
      }
    }
  }
}

// middleRight: point of right side vertically middle
// initialBitValue: initial bit value for internal state initialization
// valueSetters: array of objects with setValue(v):void function
// toggleValueProvider: object with nextValue(v):v  and previousValue(v):v functions
// labels: array of labels
cpuElements.vectorSetterBoxBuffer = function(middleRight, initialBitValue, valueSetters, toggleValueProvider, labels, uw, uh) {
  this.middleRight = middleRight.copy();
  this.unitWidth = uw || 30;
  this.unitHeight = uh || 30;
  this.topLeft = new xypoint(middleRight.x - valueSetters.length*this.unitWidth, middleRight.y - this.unitHeight/2);
  this.boxes = [];
  this.labelPlacement = 0; //0-top, 1-left, 2-bottom, 3-right, -1: don't show
  this.caption = null;
  this.captionPlacement = -1;
  this.width = this.unitWidth * valueSetters.length;
  this.internalValues = [];
  for(let i = 0; i < valueSetters.length; i++) {
    this.internalValues.push(initialBitValue);
  }
  let me = this;
  function action(i) {
    this.fire = function(info) {
      let v = me.internalValues[i];
      let leftClick = info.button == 0;
      let nv = leftClick ? toggleValueProvider.nextValue(v) : toggleValueProvider.previousValue(v);
      me.internalValues[i] = nv;
      valueSetters[i].setValue(nv);
    }
  }
  for(let i = 0; i < valueSetters.length; i++) { 
    this.boxes.push({x: this.topLeft.x+i*this.unitWidth, y: this.topLeft.y, w: this.unitWidth, h: this.unitHeight, a: new action(i)});
  }
  this.registerActiveRegions = function(activeRegions) {
    for(let i = 0; i < this.boxes.length; i++) {
      let box = this.boxes[i];
      activeRegions.add(new ActiveRegion(
        box.x, box.y, box.w, box.h, {
          execute: function(info) {box.a.fire(info);}
        }
      ));
    }
  }
  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = "#ffff00";
    for(let i = 0; i < valueSetters.length; i++) { 
      ctx.beginPath();
      ctx.rect(this.topLeft.x+i*this.unitWidth, this.topLeft.y, this.unitWidth, this.unitHeight);
      ctx.fill();
      ctx.stroke();
    }

    ctx.fillStyle = "#000000";
    ctx.font="12px Georgia";
    for(let i = 0; i < valueSetters.length; i++) { 
      ctx.textAlign = "center";
      ctx.textBaseline = "middle";
      ctx.fillText(""+this.internalValues[i], this.topLeft.x+(i+0.5)*this.unitWidth, this.topLeft.y+this.unitHeight/2);
      if(labels && labels[i]) {
        if(this.labelPlacement==0) {
          ctx.textBaseline = "bottom";
          ctx.fillText(labels[i], this.topLeft.x+(i+0.5)*this.unitWidth, this.topLeft.y-4);
        } else if(this.labelPlacement==1) {
          ctx.textAlign = "right";
          ctx.textBaseline = "middle";
          ctx.fillText(labels[i], this.topLeft.x+i*this.unitWidth-4, this.topLeft.y+this.unitHeight/2);
        } else if(this.labelPlacement==2) {
          ctx.textBaseline = "top";
          ctx.fillText(labels[i], this.topLeft.x+(i+0.5)*this.unitWidth, this.topLeft.y+this.unitHeight+4);
        } else if(this.labelPlacement==3) {
          ctx.textAlign = "left";
          ctx.textBaseline = "middle";
          ctx.fillText(labels[i], this.topLeft.x+(i+1)*this.unitWidth+4, this.topLeft.y+this.unitHeight/2);
        }
      }
    }
    if(this.caption != null && this.captionPlacement != -1) {
      ctx.textAlign = "center";
      ctx.textBaseline = "middle";
      if(this.captionPlacement==0) {
        ctx.textBaseline = "bottom";
        ctx.fillText(this.caption, this.topLeft.x+0.5*this.width, this.topLeft.y-4);
      } else if(this.captionPlacement==1) {
        ctx.textAlign = "right";
        ctx.textBaseline = "middle";
        ctx.fillText(this.caption, this.topLeft.x-4, this.topLeft.y+this.unitHeight/2);
      } else if(this.captionPlacement==2) {
        ctx.textBaseline = "top";
        ctx.fillText(this.caption, this.topLeft.x+0.5*this.width, this.topLeft.y+this.unitHeight+4);
      } else if(this.captionPlacement==3) {
        ctx.textAlign = "left";
        ctx.textBaseline = "middle";
        ctx.fillText(this.caption, this.topLeft.x+this.width+4, this.topLeft.y+this.unitHeight/2);
      }
    }
  }
}

// topLeft: position of top-left corner
// data: array of value getters
// dataMsbIsFirst: true if data[0] is MSB; false otherwise
// ce: value getter
// cp: value getter
// outputs: array of value getters
// outputsMsbIsFirst: true if outputs[0] is MSB; false otherwise
// reset: value getter
// Note: value getters are objects with no-argument function getValue() which obtain value of input/output.
cpuElements.registerFile = function(topLeft, aaddr, baddr, caddr, a, b, c, we, cp, res, contentInfo) {
  this.topLeft = topLeft.copy();
  this.width = 80;
  this.height = 230;
  this.wePoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/7);
  this.cpPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/7);
  this.aaPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*3/7);
  this.baPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*4/7);
  this.caPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*5/7);
  this.cPoint  = new xypoint(this.topLeft.x, this.topLeft.y + this.height*6/7);
  this.aPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y+this.height*1/3);
  this.bPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y+this.height*2/3);
  this.resPoint = new xypoint(this.topLeft.x+this.width/2, this.topLeft.y);
  this.hideABValues = false;
  this.bgColor = "#7CFC00";  // zelena boje travnjaka

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(""+cp.getValue(), this.cpPoint.x-4, this.cpPoint.y-4);
    ctx.fillText(""+we.getValue(), this.wePoint.x-4, this.wePoint.y-4);
    ctx.fillText(cpuElements.vgVectorToString(aaddr, true), this.aaPoint.x-4, this.aaPoint.y-4);
    ctx.fillText(cpuElements.vgVectorToString(baddr, true), this.baPoint.x-4, this.baPoint.y-4);
    ctx.fillText(cpuElements.vgVectorToString(caddr, true), this.caPoint.x-4, this.caPoint.y-4);
    ctx.fillText(cpuElements.vgVectorToString(c, true), this.cPoint.x-4, this.cPoint.y-4);

    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("WE", this.wePoint.x+4, this.wePoint.y);
    ctx.fillText("CP", this.cpPoint.x+4, this.cpPoint.y);
    ctx.fillText("Aaddr", this.aaPoint.x+4, this.aaPoint.y);
    ctx.fillText("Baddr", this.baPoint.x+4, this.baPoint.y);
    ctx.fillText("Caddr", this.caPoint.x+4, this.caPoint.y);
    ctx.fillText("C", this.cPoint.x+4, this.cPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    if(!this.hideABValues) ctx.fillText(cpuElements.vgVectorToString(a, true), this.aPoint.x+4, this.aPoint.y-4);
    if(!this.hideABValues) ctx.fillText(cpuElements.vgVectorToString(b, true), this.bPoint.x+4, this.bPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("A", this.aPoint.x-4, this.aPoint.y);
    ctx.fillText("B", this.bPoint.x-4, this.bPoint.y);

    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("registar-file", this.topLeft.x+this.width/2, this.topLeft.y+this.height-1);

    ctx.textAlign = "center";
    ctx.textBaseline = "top";
    ctx.fillText("reset", this.resPoint.x, this.resPoint.y+4);
    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(""+res.getValue(), this.resPoint.x+4, this.resPoint.y-4);

    ctx.font="12px monospace";
    ctx.textAlign = "center";
    ctx.textBaseline = "top";
    let allState = contentInfo.getState();
    for(let i = 0; i < allState.length; i++) {
      let iState = allState[i];
      let str = cpuElements.stdLogicNativeToHex(iState);
      ctx.fillText("r"+i+":"+str, this.topLeft.x+this.width*0.6, this.topLeft.y+20+i*12);
    }
  }  
}

cpuElements.chunkToHex = function(buf, i0, i1) {
    let res = 0;
    for(let i = i0; i < i1; i++) {
      res *= 2;
      if(buf[i]==2) continue;
      if(buf[i]==3) { res += 1; continue; }
      return "?";
    }
    if(res < 10) return String.fromCharCode(48+res);
    return String.fromCharCode(65+res-10);
  }

cpuElements.stdLogicNativeToHex = function(buf) {
    let res = "";
    let groups = Math.round(buf.length / 4);
    let rest = buf.length % 4;
    let chunks = [];
    if(rest != 0) chunks.push(rest);
    for(let i = 0; i <  groups; i++) {
      chunks.push(4);
    }
    let st = 0;
    for(let i = 0; i < chunks.length; i++) {
      res += cpuElements.chunkToHex(buf, st, st+chunks[i]);
      st+=chunks[i];
    }
    return res;
  }

cpuElements.alu1 = function(topLeft, a, b, apass, alurep, binv, bsel, cin, alursel, z, cout, rez) {
  this.topLeft = topLeft.copy();
  this.width = 80;
  this.height = 300;
  this.aPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/9);
  this.bPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/9);
  this.cinPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*3/9);
  this.apassPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*4/9);
  this.alurepPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*5/9);
  this.binvPoint  = new xypoint(this.topLeft.x, this.topLeft.y + this.height*6/9);
  this.bselPoint  = new xypoint(this.topLeft.x, this.topLeft.y + this.height*7/9);
  this.alurselPoint  = new xypoint(this.topLeft.x, this.topLeft.y + this.height*8/9);
  this.rezPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y+this.height*1/4);
  this.coutPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y+this.height*2/4);
  this.zPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y+this.height*3/4);
  this.hideRezValue = false;
  this.bgColor = "#FFD700";  // zlatna

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(cpuElements.vgVectorToString(a, true), this.aPoint.x-4, this.aPoint.y-4);
    ctx.fillText(cpuElements.vgVectorToString(b, true), this.bPoint.x-4, this.bPoint.y-4);
    ctx.fillText(""+cin.getValue(), this.cinPoint.x-4, this.cinPoint.y-4);
    ctx.fillText(""+apass.getValue(), this.apassPoint.x-4, this.apassPoint.y-4);
    ctx.fillText(""+alurep.getValue(), this.alurepPoint.x-4, this.alurepPoint.y-4);
    ctx.fillText(""+binv.getValue(), this.binvPoint.x-4, this.binvPoint.y-4);
    ctx.fillText(""+bsel.getValue(), this.bselPoint.x-4, this.bselPoint.y-4);
    ctx.fillText(""+alursel.getValue(), this.alurselPoint.x-4, this.alurselPoint.y-4);

    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("A", this.aPoint.x+4, this.aPoint.y);
    ctx.fillText("B", this.bPoint.x+4, this.bPoint.y);
    ctx.fillText("cin", this.cinPoint.x+4, this.cinPoint.y);
    ctx.fillText("apass", this.apassPoint.x+4, this.apassPoint.y);
    ctx.fillText("alurep", this.alurepPoint.x+4, this.alurepPoint.y);
    ctx.fillText("binv", this.binvPoint.x+4, this.binvPoint.y);
    ctx.fillText("bsel", this.bselPoint.x+4, this.bselPoint.y);
    ctx.fillText("alursel", this.alurselPoint.x+4, this.alurselPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    if(!this.hideRezValue) ctx.fillText(cpuElements.vgVectorToString(rez, true), this.rezPoint.x+4, this.rezPoint.y-4);
    ctx.fillText(""+cout.getValue(), this.coutPoint.x+4, this.coutPoint.y-4);
    ctx.fillText(""+z.getValue(), this.zPoint.x+4, this.zPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("rez", this.rezPoint.x-4, this.rezPoint.y);
    ctx.fillText("cout", this.coutPoint.x-4, this.coutPoint.y);
    ctx.fillText("z", this.zPoint.x-4, this.zPoint.y);

    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("ALU", this.topLeft.x+this.width/2, this.topLeft.y+this.height-1);
  }
}

cpuElements.programCounter = function(topLeft, data, pcsel, res, ce, cp, address) {
  this.topLeft = topLeft.copy();
  this.width = 90;
  this.height = 180;
  this.dataPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/5);
  this.pcselPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/5);
  this.cePoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*3/5);
  this.cpPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*4/5);
  this.addressPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.height/2);
  this.resPoint = new xypoint(this.topLeft.x+this.width/2, this.topLeft.y);
  this.bgColor = "#FFB6C1"; // light pink

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(cpuElements.vgVectorToString(data, true), this.dataPoint.x-4, this.dataPoint.y-4);
    ctx.fillText(""+pcsel.getValue(), this.pcselPoint.x-4, this.pcselPoint.y-4);
    ctx.fillText(""+cp.getValue(), this.cpPoint.x-4, this.cpPoint.y-4);
    ctx.fillText(""+ce.getValue(), this.cePoint.x-4, this.cePoint.y-4);

    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("data", this.dataPoint.x+4, this.dataPoint.y);
    ctx.fillText("CP", this.cpPoint.x+4, this.cpPoint.y);
    ctx.fillText("CE", this.cePoint.x+4, this.cePoint.y);
    ctx.fillText("pcsel", this.pcselPoint.x+4, this.pcselPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(cpuElements.vgVectorToString(address, true), this.addressPoint.x+4, this.addressPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("address", this.addressPoint.x-4, this.addressPoint.y);

    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("program counter", this.topLeft.x+this.width/2, this.topLeft.y+this.height-1);

    ctx.textAlign = "center";
    ctx.textBaseline = "top";
    ctx.fillText("reset", this.resPoint.x, this.resPoint.y+4);
    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(""+res.getValue(), this.resPoint.x+4, this.resPoint.y-4);
  }
}

cpuElements.rippleCarryAdder = function(topLeft, a, b, cin, s, cout) {
  this.topLeft = topLeft.copy();
  this.hideAB = false;
  this.width = 90;
  this.height = 180;
  this.aPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/3);
  this.bPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/3);
  this.cinPoint = new xypoint(this.topLeft.x+this.width/2, this.topLeft.y + this.height);
  this.coutPoint = new xypoint(this.topLeft.x+this.width/2, this.topLeft.y);
  this.sPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.height/2);
  this.bgColor = "#FF7F50";  // coral

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.font="12px Georgia";
    ctx.fillStyle = "#000000";
    if(!this.hideAB) {
      ctx.textAlign = "right";
      ctx.textBaseline = "bottom";
      ctx.fillText(cpuElements.vgVectorToString(a, true), this.aPoint.x-4, this.aPoint.y-4);
      ctx.fillText(cpuElements.vgVectorToString(b, true), this.bPoint.x-4, this.bPoint.y-4);
    }

    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("A", this.aPoint.x+4, this.aPoint.y);
    ctx.fillText("B", this.bPoint.x+4, this.bPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(cpuElements.vgVectorToString(s, true), this.sPoint.x+4, this.sPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("s", this.sPoint.x-4, this.sPoint.y);

    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("RCA", this.topLeft.x+this.width/2, this.topLeft.y+this.height-1-25);

    ctx.textAlign = "center";
    ctx.textBaseline = "top";
    ctx.fillText("cout", this.coutPoint.x, this.coutPoint.y+4);
    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(""+cout.getValue(), this.coutPoint.x+4, this.coutPoint.y-4);

    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("cin", this.coutPoint.x, this.coutPoint.y + this.height-4);
    ctx.textAlign = "left";
    ctx.textBaseline = "top";
    ctx.fillText(""+cin.getValue(), this.cinPoint.x+4, this.cinPoint.y+4);
  }
}

cpuElements.fixedLabel = function(p, halign, valign, text) {
  this.p = p.copy();

  this.draw = function(ctx) {
    ctx.font="12px Georgia";
    ctx.textAlign = halign;
    ctx.textBaseline = valign;
    ctx.fillStyle = "#000000";
    ctx.fillText(text, p.x, p.y);
  }
}

cpuElements.passElement = function(topLeft, data, control, output) {
  this.topLeft = topLeft.copy();
  this.width = 70;
  this.height = 90;
  this.dataPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/3);
  this.controlPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/3);
  this.outputPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.height/2);
  this.bgColor = "#98FB98";

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(cpuElements.vgVectorToString(data, true), this.dataPoint.x-4, this.dataPoint.y-4);
    ctx.fillText(""+control.getValue(), this.controlPoint.x-4, this.controlPoint.y-4);

    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("DATA", this.dataPoint.x+4, this.dataPoint.y);
    ctx.fillText("control", this.controlPoint.x+4, this.controlPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(cpuElements.vgVectorToString(output, true), this.outputPoint.x+4, this.outputPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("out", this.outputPoint.x-4, this.outputPoint.y);

    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("Pass", this.topLeft.x+this.width/2, this.topLeft.y+this.height-1);
  }
}

cpuElements.controlledInverterElement = function(topLeft, data, control, output) {
  this.topLeft = topLeft.copy();
  this.width = 70;
  this.height = 90;
  this.dataPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/3);
  this.controlPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/3);
  this.outputPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.height/2);
  this.bgColor = "#FFE4B5"; // moccasin

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(cpuElements.vgVectorToString(data, true), this.dataPoint.x-4, this.dataPoint.y-4);
    ctx.fillText(""+control.getValue(), this.controlPoint.x-4, this.controlPoint.y-4);

    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("DATA", this.dataPoint.x+4, this.dataPoint.y);
    ctx.fillText("control", this.controlPoint.x+4, this.controlPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(cpuElements.vgVectorToString(output, true), this.outputPoint.x+4, this.outputPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("out", this.outputPoint.x-4, this.outputPoint.y);

    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("C. Inverter", this.topLeft.x+this.width/2, this.topLeft.y+this.height-1);
  }
}

cpuElements.zeroTestElement = function(topLeft, data, output) {
  this.topLeft = topLeft.copy();
  this.width = 70;
  this.height = 70;
  this.dataPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/2);
  this.outputPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.height/2);
  this.bgColor = "#87CEFA"; // lightskyblue

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(cpuElements.vgVectorToString(data, true), this.dataPoint.x-4, this.dataPoint.y-4);

    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("DATA", this.dataPoint.x+4, this.dataPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(""+output.getValue(), this.outputPoint.x+4, this.outputPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("out", this.outputPoint.x-4, this.outputPoint.y);

    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("Zero Test", this.topLeft.x+this.width/2, this.topLeft.y+this.height-1);
  }
}

cpuElements.replicateElement = function(topLeft, data, output) {
  this.topLeft = topLeft.copy();
  this.width = 70;
  this.height = 90;
  this.dataPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/2);
  this.outputPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.height/2);
  this.bgColor = "#DDA0DD";  // plum

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(""+data.getValue(), this.dataPoint.x-4, this.dataPoint.y-4);

    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("DATA", this.dataPoint.x+4, this.dataPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(cpuElements.vgVectorToString(output, true), this.outputPoint.x+4, this.outputPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("out", this.outputPoint.x-4, this.outputPoint.y);

    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("Replicate", this.topLeft.x+this.width/2, this.topLeft.y+this.height-1);
  }
}

cpuElements.andVectElement = function(topLeft, a, b, output) {
  this.topLeft = topLeft.copy();
  this.width = 70;
  this.height = 90;
  this.aPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/3);
  this.bPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/3);
  this.outputPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.height/2);
  this.bgColor = "#FFC0CB";  //pink

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(cpuElements.vgVectorToString(a, true), this.aPoint.x-4, this.aPoint.y-4);
    ctx.fillText(cpuElements.vgVectorToString(b, true), this.bPoint.x-4, this.bPoint.y-4);

    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("a", this.aPoint.x+4, this.aPoint.y);
    ctx.fillText("b", this.bPoint.x+4, this.bPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(cpuElements.vgVectorToString(output, true), this.outputPoint.x+4, this.outputPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("y", this.outputPoint.x-4, this.outputPoint.y);

    ctx.font="24px Georgia";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText("&", this.topLeft.x+this.width/2, this.topLeft.y+this.height/2);
  }
}

// Logički I; ulaze prikazuje žicu po žicu
// topLeft: gornji lijevi ugao
// inputs: polje ulaza
// output: izlaz
cpuElements.andElement = function(topLeft, inputs, output) {
  this.topLeft = topLeft.copy();
  this.width = 50;
  this.height = 50;
  this.inputPoint = [];
  this.outputPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.height/2);
  this.bgColor = "#F0FFFF"; // azurna

  for(let i = 0; i < inputs.length; i++) {
    this.inputPoint.push(new xypoint(this.topLeft.x, this.topLeft.y + this.height*(i+1)/(inputs.length+1)));
  }

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    for(let i = 0; i < inputs.length; i++) {
      ctx.fillText(""+inputs[i].getValue(), this.inputPoint[i].x-4, this.inputPoint[i].y);
    }

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(""+output.getValue(), this.outputPoint.x+4, this.outputPoint.y-4);

    ctx.font="24px Georgia";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText("&", this.topLeft.x+this.width/2, this.topLeft.y+this.height/2);
  }
}

cpuElements.ctrlUnit1Element = function(topLeft, cp, ir, rc, z, c, 
    sig_mar_we, sig_dataio_re, sig_dataio_wr, memoryRequest, memoryRW, sig_ir_we, sig_ira_we, sig_pc_ce, sig_pc_sel,
    sig_mux_mar_sel, sig_regfile_addr_a, sig_regfile_addr_b, sig_regfile_addr_c, sig_regfile_we,
    sig_mux_regfile_c_sel, 
    sig_alu_cin, sig_alu_pass, sig_alu_rep, sig_alu_binv, sig_alu_bsel, sig_alu_rsel,
    sig_zeroff_we, sig_carryff_we, sig_cpDisable
) {
  this.topLeft = topLeft.copy();
  this.width = 300;
  this.height = 200;
  this.topOff = 10;
  this.bgColor = "#FF7777";

  this.sig_cpDisablePoint = new xypoint(this.topLeft.x, this.topLeft.y + this.topOff+(this.height-this.topOff)*1/13);
  this.sig_mar_wePoint = new xypoint(this.topLeft.x, this.topLeft.y + this.topOff+(this.height-this.topOff)*2/13);
  this.sig_dataio_rePoint = new xypoint(this.topLeft.x, this.topLeft.y + this.topOff+(this.height-this.topOff)*3/13);
  this.sig_dataio_wrPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.topOff+(this.height-this.topOff)*4/13);
  this.memoryRequestPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.topOff+(this.height-this.topOff)*5/13);
  this.memoryRWPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.topOff+(this.height-this.topOff)*6/13);
  this.sig_ir_wePoint = new xypoint(this.topLeft.x, this.topLeft.y + this.topOff+(this.height-this.topOff)*7/13);
  this.sig_ira_wePoint = new xypoint(this.topLeft.x, this.topLeft.y + this.topOff+(this.height-this.topOff)*8/13);
  this.sig_pc_cePoint = new xypoint(this.topLeft.x, this.topLeft.y + this.topOff+(this.height-this.topOff)*9/13);
  this.sig_pc_selPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.topOff+(this.height-this.topOff)*10/13);
  this.sig_mux_mar_selPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.topOff+(this.height-this.topOff)*11/13);
  this.sig_mux_regfile_c_selPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.topOff+(this.height-this.topOff)*12/13);

  this.sig_regfile_addr_aPoint = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.topOff+(this.height-this.topOff)*1/13);
  this.sig_regfile_addr_bPoint = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.topOff+(this.height-this.topOff)*2/13);
  this.sig_regfile_addr_cPoint = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.topOff+(this.height-this.topOff)*3/13);
  this.sig_regfile_wePoint = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.topOff+(this.height-this.topOff)*4/13);
  this.sig_alu_cinPoint = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.topOff+(this.height-this.topOff)*5/13);
  this.sig_alu_passPoint = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.topOff+(this.height-this.topOff)*6/13);
  this.sig_alu_repPoint = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.topOff+(this.height-this.topOff)*7/13);
  this.sig_alu_binvPoint = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.topOff+(this.height-this.topOff)*8/13);
  this.sig_alu_bselPoint = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.topOff+(this.height-this.topOff)*9/13);
  this.sig_alu_rselPoint = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.topOff+(this.height-this.topOff)*10/13);
  this.sig_zeroff_wePoint = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.topOff+(this.height-this.topOff)*11/13);
  this.sig_carryff_wePoint = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.topOff+(this.height-this.topOff)*12/13);

  this.cpPoint = new xypoint(this.topLeft.x+this.width*1/6, this.topLeft.y);
  this.irPoint = new xypoint(this.topLeft.x+this.width*2/6, this.topLeft.y);
  this.rcPoint = new xypoint(this.topLeft.x+this.width*3/6, this.topLeft.y);
  this.zPoint = new xypoint(this.topLeft.x+this.width*4/6, this.topLeft.y);
  this.cPoint = new xypoint(this.topLeft.x+this.width*5/6, this.topLeft.y);

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.fillStyle = "#000000";
    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText(""+sig_cpDisable.getValue(), this.sig_cpDisablePoint.x-4, this.sig_cpDisablePoint.y);
    ctx.fillText(""+sig_mar_we.getValue(), this.sig_mar_wePoint.x-4, this.sig_mar_wePoint.y);
    ctx.fillText(""+sig_dataio_re.getValue(), this.sig_dataio_rePoint.x-4, this.sig_dataio_rePoint.y);
    ctx.fillText(""+sig_dataio_wr.getValue(), this.sig_dataio_wrPoint.x-4, this.sig_dataio_wrPoint.y);
    ctx.fillText(""+memoryRequest.getValue(), this.memoryRequestPoint.x-4, this.memoryRequestPoint.y);
    ctx.fillText(""+memoryRW.getValue(), this.memoryRWPoint.x-4, this.memoryRWPoint.y);
    ctx.fillText(""+sig_ir_we.getValue(), this.sig_ir_wePoint.x-4, this.sig_ir_wePoint.y);
    ctx.fillText(""+sig_ira_we.getValue(), this.sig_ira_wePoint.x-4, this.sig_ira_wePoint.y);
    ctx.fillText(""+sig_pc_ce.getValue(), this.sig_pc_cePoint.x-4, this.sig_pc_cePoint.y);
    ctx.fillText(""+sig_pc_sel.getValue(), this.sig_pc_selPoint.x-4, this.sig_pc_selPoint.y);
    ctx.fillText(cpuElements.vgVectorToString(sig_mux_mar_sel,true), this.sig_mux_mar_selPoint.x-4, this.sig_mux_mar_selPoint.y);
    ctx.fillText(""+sig_mux_regfile_c_sel.getValue(), this.sig_mux_regfile_c_selPoint.x-4, this.sig_mux_regfile_c_selPoint.y);
    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("cpDisable", this.sig_cpDisablePoint.x+4, this.sig_cpDisablePoint.y);
    ctx.fillText("mar_we", this.sig_mar_wePoint.x+4, this.sig_mar_wePoint.y);
    ctx.fillText("dataio_re", this.sig_dataio_rePoint.x+4, this.sig_dataio_rePoint.y);
    ctx.fillText("dataio_wr", this.sig_dataio_wrPoint.x+4, this.sig_dataio_wrPoint.y);
    ctx.fillText("memoryRequest", this.memoryRequestPoint.x+4, this.memoryRequestPoint.y);
    ctx.fillText("memoryRW", this.memoryRWPoint.x+4, this.memoryRWPoint.y);
    ctx.fillText("ir_we", this.sig_ir_wePoint.x+4, this.sig_ir_wePoint.y);
    ctx.fillText("ira_we", this.sig_ira_wePoint.x+4, this.sig_ira_wePoint.y);
    ctx.fillText("pc_ce", this.sig_pc_cePoint.x+4, this.sig_pc_cePoint.y);
    ctx.fillText("pc_sel", this.sig_pc_selPoint.x+4, this.sig_pc_selPoint.y);
    ctx.fillText("mar_sel", this.sig_mux_mar_selPoint.x+4, this.sig_mux_mar_selPoint.y);
    ctx.fillText("regfile_c_sel", this.sig_mux_regfile_c_selPoint.x+4, this.sig_mux_regfile_c_selPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(""+cp.getValue(), this.cpPoint.x+4, this.cpPoint.y-4);
    //ctx.fillText(cpuElements.vgVectorToString(ir,true), this.irPoint.x+4, this.irPoint.y-4);
    //ctx.fillText(cpuElements.vgVectorToString(rc,true), this.rcPoint.x+4, this.rcPoint.y-4);
    ctx.fillText(""+z.getValue(), this.zPoint.x+4, this.zPoint.y-4);
    ctx.fillText(""+c.getValue(), this.cPoint.x+4, this.cPoint.y-4);
    ctx.textAlign = "center";
    ctx.textBaseline = "top";
    ctx.fillText("CP", this.cpPoint.x, this.cpPoint.y+4);
    ctx.fillText("IR", this.irPoint.x, this.irPoint.y+4);
    ctx.fillText("Q", this.rcPoint.x, this.rcPoint.y+4);
    ctx.fillText("Z", this.zPoint.x, this.zPoint.y+4);
    ctx.fillText("C", this.cPoint.x, this.cPoint.y+4);

    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText(cpuElements.vgVectorToString(sig_regfile_addr_a,true), this.sig_regfile_addr_aPoint.x+4, this.sig_regfile_addr_aPoint.y);
    ctx.fillText(cpuElements.vgVectorToString(sig_regfile_addr_b,true), this.sig_regfile_addr_bPoint.x+4, this.sig_regfile_addr_bPoint.y);
    ctx.fillText(cpuElements.vgVectorToString(sig_regfile_addr_c,true), this.sig_regfile_addr_cPoint.x+4, this.sig_regfile_addr_cPoint.y);
    ctx.fillText(""+sig_regfile_we.getValue(), this.sig_regfile_wePoint.x+4, this.sig_regfile_wePoint.y);
    ctx.fillText(""+sig_alu_cin.getValue(), this.sig_alu_cinPoint.x+4, this.sig_alu_cinPoint.y);
    ctx.fillText(""+sig_alu_pass.getValue(), this.sig_alu_passPoint.x+4, this.sig_alu_passPoint.y);
    ctx.fillText(""+sig_alu_rep.getValue(), this.sig_alu_repPoint.x+4, this.sig_alu_repPoint.y);
    ctx.fillText(""+sig_alu_binv.getValue(), this.sig_alu_binvPoint.x+4, this.sig_alu_binvPoint.y);
    ctx.fillText(""+sig_alu_bsel.getValue(), this.sig_alu_bselPoint.x+4, this.sig_alu_bselPoint.y);
    ctx.fillText(""+sig_alu_rsel.getValue(), this.sig_alu_rselPoint.x+4, this.sig_alu_rselPoint.y);
    ctx.fillText(""+sig_zeroff_we.getValue(), this.sig_zeroff_wePoint.x+4, this.sig_zeroff_wePoint.y);
    ctx.fillText(""+sig_carryff_we.getValue(), this.sig_carryff_wePoint.x+4, this.sig_carryff_wePoint.y);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("regfile_addr_a", this.sig_regfile_addr_aPoint.x-4, this.sig_regfile_addr_aPoint.y);
    ctx.fillText("regfile_addr_b", this.sig_regfile_addr_bPoint.x-4, this.sig_regfile_addr_bPoint.y);
    ctx.fillText("regfile_addr_c", this.sig_regfile_addr_cPoint.x-4, this.sig_regfile_addr_cPoint.y);
    ctx.fillText("regfile_we", this.sig_regfile_wePoint.x-4, this.sig_regfile_wePoint.y);
    ctx.fillText("alu_cin", this.sig_alu_cinPoint.x-4, this.sig_alu_cinPoint.y);
    ctx.fillText("alu_apass", this.sig_alu_passPoint.x-4, this.sig_alu_passPoint.y);
    ctx.fillText("alu_rep", this.sig_alu_repPoint.x-4, this.sig_alu_repPoint.y);
    ctx.fillText("alu_binv", this.sig_alu_binvPoint.x-4, this.sig_alu_binvPoint.y);
    ctx.fillText("alu_bsel", this.sig_alu_bselPoint.x-4, this.sig_alu_bselPoint.y);
    ctx.fillText("alu_rsel", this.sig_alu_rselPoint.x-4, this.sig_alu_rselPoint.y);
    ctx.fillText("zeroff_we", this.sig_zeroff_wePoint.x-4, this.sig_zeroff_wePoint.y);
    ctx.fillText("carryff_we", this.sig_carryff_wePoint.x-4, this.sig_carryff_wePoint.y);

    ctx.font="14px Georgia";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText("Upravljačka jedinica", this.topLeft.x+this.width/2, this.topLeft.y+this.height/2);
  }
}

cpuElements.dffElement = function(topLeft, d, cp, ce, set, reset, q, qn) {
  this.topLeft = topLeft.copy();
  this.width = 70;
  this.height = 90;
  this.dPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/4);
  this.cpPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/4);
  this.icpPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/4);
  this.cePoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*3/4);
  this.qPoint = new xypoint(this.topLeft.x + this.width, this.topLeft.y + this.height*1/4);
  this.qnPoint = new xypoint(this.topLeft.x + this.width, this.topLeft.y + this.height*3/4);
  this.setPoint = new xypoint(this.topLeft.x + this.width/2, this.topLeft.y);
  this.resetPoint = new xypoint(this.topLeft.x + this.width/2, this.topLeft.y+this.height);
  this.bgColor = "#CCCCFF";
  this.clockActivation = 0; // 0: rising_edge, 1: falling_edge, 2: high-level, 3: low-level
  let r = 3;

  this.setClockActivation = function(ca) {
    this.clockActivation = ca;
    if(ca==1 || ca==3) {
      this.cpPoint.x = this.icpPoint.x-2*r;
    } else {
      this.cpPoint.x = this.icpPoint.x;
    }
  }
  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    if(this.clockActivation==0 || this.clockActivation==1) {
      let dx = 4;
      let dy = 5;
      ctx.beginPath();
      ctx.moveTo(this.icpPoint.x, this.icpPoint.y-dy);
      ctx.lineTo(this.icpPoint.x+dx, this.icpPoint.y);
      ctx.lineTo(this.icpPoint.x, this.icpPoint.y+dy);
      ctx.stroke();
    }
    if(this.clockActivation==1 || this.clockActivation==3) {
      ctx.fillStyle = "#FFFFFF";
      ctx.beginPath();
      ctx.arc(this.icpPoint.x-r, this.icpPoint.y, r, 0, Math.PI*2, true);
      ctx.fill();
      ctx.stroke();
    }

    ctx.fillStyle = "#000000";
    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillText(""+d.getValue(), this.dPoint.x-4, this.dPoint.y-4);
    ctx.fillText(""+cp.getValue(), this.icpPoint.x-4, this.icpPoint.y-4);
    if(ce!=null) ctx.fillText(""+ce.getValue(), this.cePoint.x-4, this.cePoint.y-4);
    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("D", this.dPoint.x+4, this.dPoint.y);
    ctx.fillText("CP", this.icpPoint.x+4, this.icpPoint.y);
    if(ce!=null) ctx.fillText("CE", this.cePoint.x+4, this.cePoint.y);

    if(set != null) {
      ctx.textAlign = "left";
      ctx.textBaseline = "bottom";
      ctx.fillText(""+set.getValue(), this.setPoint.x+4, this.setPoint.y-4);
      ctx.textAlign = "center";
      ctx.textBaseline = "top";
      ctx.fillText("SET", this.setPoint.x, this.setPoint.y+4);
    }

    if(reset != null) {
      ctx.textAlign = "left";
      ctx.textBaseline = "top";
      ctx.fillText(""+reset.getValue(), this.resetPoint.x+4, this.resetPoint.y+4);
      ctx.textAlign = "center";
      ctx.textBaseline = "bottom";
      ctx.fillText("RESET", this.resetPoint.x, this.resetPoint.y-4);
    }

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(""+q.getValue(), this.qPoint.x+4, this.qPoint.y-4);
    if(qn!=null) ctx.fillText(""+qn.getValue(), this.qnPoint.x+4, this.qnPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("Q", this.qPoint.x-4, this.qPoint.y);
    if(qn!=null) ctx.fillText("Qn", this.qnPoint.x-4, this.qnPoint.y);

    ctx.font="24px Georgia";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText("FF", this.topLeft.x+this.width/2, this.topLeft.y+this.height/2);
  }
}

cpuElements.ringCounterElement = function(topLeft, cp, reset, q) {
  this.topLeft = topLeft.copy();
  this.width = 250;
  this.height = 60;
  this.cpPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/3);
  this.resetPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/3);
  this.qPoint = new xypoint(this.topLeft.x + this.width/2, this.topLeft.y + this.height);
  this.bgColor = "#FF00FF"; // magenta

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.fillStyle = "#000000";
    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillText(""+reset.getValue(), this.resetPoint.x-4, this.resetPoint.y-4);
    ctx.fillText(""+cp.getValue(), this.cpPoint.x-4, this.cpPoint.y-4);
    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("RESET", this.resetPoint.x+4, this.resetPoint.y);
    ctx.fillText("CP", this.cpPoint.x+4, this.cpPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "top";
    ctx.fillText(cpuElements.vgVectorToString(q, true), this.qPoint.x+4, this.qPoint.y+4);
    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("Q", this.qPoint.x, this.qPoint.y-4);

    ctx.font="16px Georgia";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText("Prstenasto brojilo", this.topLeft.x+this.width/2, this.topLeft.y+this.height/2);
  }
}

cpuElements.ramElement = function(topLeft, addr, rwn, cs0, cs1, data) {
  this.topLeft = topLeft.copy();
  this.width = 70;
  this.height = 170;
  this.label = "RAM";
  this.rwnPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/4);
  this.cs0Point = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/4);
  this.cs1Point = new xypoint(this.topLeft.x, this.topLeft.y + this.height*3/4);
  this.dataPoint  = new xypoint(this.topLeft.x+this.width/2, this.topLeft.y);
  this.addrPoint  = new xypoint(this.topLeft.x+this.width/2, this.topLeft.y + this.height);

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = "#ffffff";
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(""+rwn.getValue(), this.rwnPoint.x-4, this.rwnPoint.y-4);
    ctx.fillText(""+cs0.getValue(), this.cs0Point.x-4, this.cs0Point.y-4);
    ctx.fillText(""+cs1.getValue(), this.cs1Point.x-4, this.cs1Point.y-4);
    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("RW'", this.rwnPoint.x+4, this.rwnPoint.y);
    ctx.fillText("CS0", this.cs0Point.x+4, this.cs0Point.y);
    ctx.fillText("CS1", this.cs1Point.x+4, this.cs1Point.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(cpuElements.vgVectorToString(data, true), this.dataPoint.x+4, this.dataPoint.y-4);
    ctx.textAlign = "center";
    ctx.textBaseline = "top";
    ctx.fillText("DATA", this.dataPoint.x, this.dataPoint.y+4);

    ctx.textAlign = "left";
    ctx.textBaseline = "top";
    ctx.fillText(cpuElements.vgVectorToString(addr, true), this.addrPoint.x+4, this.addrPoint.y+4);
    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("ADDRESS", this.addrPoint.x, this.addrPoint.y-4);

    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText(this.label, this.topLeft.x+this.width*2/3, this.topLeft.y+this.height/2);
  }
}

cpuElements.ramContentElement = function(topLeft, circuit) {
  this.topLeft = topLeft.copy();
  this.addressWidth = 30;
  this.unitWidth = 20;
  this.unitHeight = 20;
  this.width = (circuit.addressBits>5 ? 32 : Math.pow(2,circuit.addressBits))*this.unitWidth + this.addressWidth;
  this.height = (circuit.addressBits>5 ? Math.pow(2,circuit.addressBits-5) : 1)*this.unitHeight;

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = "#ffffff";
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();
    ctx.beginPath();
    ctx.moveTo(this.addressWidth-1, 0);
    ctx.lineTo(this.addressWidth-1, this.height);
    ctx.stroke();

    ctx.font="10px Monospaced";
    ctx.fillStyle = "#000000";
    let rows = circuit.addressBits>5 ? Math.pow(2,circuit.addressBits-5) : 1;
    let addr = 0; let addrs = Math.pow(2, circuit.addressBits);
    for(let r = 0; r < rows; r++) {
      ctx.textAlign = "right";
      ctx.textBaseline = "middle";
      ctx.fillText(addr.toString(16)+":", this.addressWidth-2, (r+0.5)*this.unitHeight);
      ctx.textAlign = "center";
      ctx.textBaseline = "middle";
      for(let c = 0; c < 32; c++) {
        let word = circuit.content[addr];
        if(circuit.lastOperation != -1 && circuit.lastOperationAddress==addr) {
          ctx.fillStyle = circuit.lastOperation==0 ? "#FFAAAA" : "#AAFFAA";
          ctx.beginPath();
          ctx.rect(this.addressWidth+c*this.unitWidth, r*this.unitHeight, this.unitWidth, this.unitHeight);
          ctx.fill();
          ctx.fillStyle = "#000000";
        }
        let text = cpuElements.stdLogicNativeToHex(word);
        ctx.fillText(text, this.addressWidth+(c+0.5)*this.unitWidth, (r+0.5)*this.unitHeight);
        addr++;
        if(addr >= addrs) break;
      }
    }
  }
}

cpuElements.cup1InfoElement = function(topLeft, circuit) {
  this.topLeft = topLeft.copy();
  this.width = 200;
  this.height = 270;
  this.label = "CPU";
  this.r0Point = new xypoint(this.topLeft.x + 55, this.topLeft.y+20);
  this.r1Point = this.r0Point.copy().translate(0, 20);
  this.r2Point = this.r0Point.copy().translate(0, 40);
  this.r3Point = this.r0Point.copy().translate(0, 60);
  this.cyclePoint = this.r0Point.copy().translate(0, 80);
  this.pcPoint = this.r0Point.copy().translate(0, 100);
  this.irPoint = this.r0Point.copy().translate(0, 120);
  this.iraPoint = this.r0Point.copy().translate(0, 140);
  this.marPoint = this.r0Point.copy().translate(0, 160);
  this.instrPoint = this.r0Point.copy().translate(0, 180);
  this.czPoint = new xypoint(this.topLeft.x + this.width - 70, this.topLeft.y+20);

  this.cpPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/3);
  this.resPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/3);

  this.rwPoint = new xypoint(this.topLeft.x + this.width, this.topLeft.y + this.height*1/3);
  this.mrPoint = new xypoint(this.topLeft.x + this.width, this.topLeft.y + this.height*2/3);
  this.dataPoint  = new xypoint(this.topLeft.x+this.width/2, this.topLeft.y);
  this.addrPoint  = new xypoint(this.topLeft.x+this.width/2, this.topLeft.y + this.height);

  function decodeCycle(ci) {
    if(ci.err) return "?";
    return "T"+ci.index;
  }
  function cycleIndex(sig) {
    let ind = -1;
    let err = false;
    for(let i = 0; i < sig.length; i++) {
      if(sig[i].value == 2) {continue;}
      if(sig[i].value == 3) {if(ind==-1) ind = i; else err = true; continue;}
      err = true;
    }
    return {err: err, index: ind+1};
  }
  function stdlogVectorToInt(sig, startIndex, endIndex) {
    let res = 0;
    for(let i = startIndex; i < endIndex; i++) {
      res *= 2;
      if(sig[i].value == 2) continue;
      if(sig[i].value == 3) { res += 1; continue; }
      return -1;
    }
    return res;
  }

  function toRegisterName(sig, startIndex, endIndex) {
    let index = stdlogVectorToInt(sig, startIndex, endIndex);
    if(index<0) return "???";
    return "r"+index;
  }

  function toNumberValueEx(condition, sig, startIndex, endIndex) {
    if(!condition) return "???";
    let index = stdlogVectorToInt(sig, startIndex, endIndex);
    if(index<0) return "???";
    return index;
  }
  function toNumberValue(sig, startIndex, endIndex) {
    let index = stdlogVectorToInt(sig, startIndex, endIndex);
    if(index<0) return "???";
    return index;
  }

  function stdlogArrayToString(array, zeroIsMSB) {
    var res = [];
    if(zeroIsMSB) {
      for(let i = 0; i < array.length; i++) res.push(dlsimul.std_logic_1164.values[array[i]]);
    } else {
      for(let i = array.length-1; i >= 0; i--) res.push(dlsimul.std_logic_1164.values[array[i]]);
    }
    return res.join("");
  }
  function stdlogVectorToString(signal, zeroIsMSB) {
    var res = [];
    if(zeroIsMSB) {
      for(let i = 0; i < signal.length; i++) res.push(dlsimul.std_logic_1164.values[signal[i].value]);
    } else {
      for(let i = signal.length-1; i >= 0; i--) res.push(dlsimul.std_logic_1164.values[signal[i].value]);
    }
    return res.join("");
  }
  function stdlogToString(signal) {
    return dlsimul.std_logic_1164.values[signal.value];
  }
  function startsWith(arr, data) {
    for(let i = 0; i < data.length; i++) if(arr[i].value != data[i]) return false;
    return true;
  }
  function decodeInstruction(ci, ir_data, ira_data) {
    if(ci.err || ci.index < 3) return null;
    if(startsWith(ir_data, [2,2,2,2])) return "MOV "+toRegisterName(ir_data,4,6)+", "+toRegisterName(ir_data,6,8);
    if(startsWith(ir_data, [2,2,2,3])) return "ADD "+toRegisterName(ir_data,4,6)+", "+toRegisterName(ir_data,6,8);
    if(startsWith(ir_data, [2,2,3,2])) return "SUB "+toRegisterName(ir_data,4,6)+", "+toRegisterName(ir_data,6,8);
    if(startsWith(ir_data, [2,2,3,3])) return "AND "+toRegisterName(ir_data,4,6)+", "+toRegisterName(ir_data,6,8);
    if(startsWith(ir_data, [2,3,2,2])) return "INV "+toRegisterName(ir_data,4,6)+", "+toRegisterName(ir_data,6,8);
    if(startsWith(ir_data, [3,2,2,2,2,2])) return "LDI "+toRegisterName(ir_data,6,8)+", "+toNumberValueEx(ci.index>=5,ira_data,0,8);
    if(startsWith(ir_data, [3,2,2,2,2,3])) return "LDA "+toRegisterName(ir_data,6,8)+", ["+toNumberValueEx(ci.index>=5,ira_data,0,8)+"]";
    if(startsWith(ir_data, [3,2,2,2,3,2])) return "STA ["+toNumberValueEx(ci.index>=5,ira_data,0,8)+"], "+toRegisterName(ir_data,6,8);
    if(startsWith(ir_data, [3,2,2,3])) return "LD "+toRegisterName(ir_data,4,6)+", ["+toRegisterName(ir_data,6,8)+"]";
    if(startsWith(ir_data, [3,2,3,2])) return "ST ["+toRegisterName(ir_data,6,8)+"], "+toRegisterName(ir_data,4,6);
    if(startsWith(ir_data, [3,2,3,3])) return "INC "+toRegisterName(ir_data,4,6);
    if(startsWith(ir_data, [3,3,2,2])) return "DEC "+toRegisterName(ir_data,4,6);
    if(startsWith(ir_data, [3,3,2,3])) return "TESTZ "+toRegisterName(ir_data,4,6);
    if(startsWith(ir_data, [3,3,3,2,2,2])) return "JMP "+toNumberValueEx(ci.index>=5,ira_data,0,8);
    if(startsWith(ir_data, [3,3,3,2,2,3])) return "JZ "+toNumberValueEx(ci.index>=5,ira_data,0,8);
    if(startsWith(ir_data, [3,3,3,2,3,2])) return "JC "+toNumberValueEx(ci.index>=5,ira_data,0,8);
    if(startsWith(ir_data, [3,3,3,3,3,3,3,3])) return "HALT";
  }

  let instrDescCache = null;

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = "#ffffff";
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.font="12px Georgia";
    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(stdlogToString(circuit.rw), this.rwPoint.x+4, this.rwPoint.y-4);
    ctx.fillText(stdlogToString(circuit.mr), this.mrPoint.x+4, this.mrPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("RW'", this.rwPoint.x-4, this.rwPoint.y);
    ctx.fillText("MR", this.mrPoint.x-4, this.mrPoint.y);

    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(stdlogToString(circuit.reset), this.resPoint.x-4, this.resPoint.y-4);
    ctx.fillText(stdlogToString(circuit.cp), this.cpPoint.x-4, this.cpPoint.y-4);
    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("RESET", this.resPoint.x+4, this.resPoint.y);
    ctx.fillText("CP", this.cpPoint.x+4, this.cpPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "top";
    ctx.fillStyle = "#000000";
    let regst = circuit.regfile.getState();
    ctx.fillText("r0: " + cpuElements.stdLogicNativeToHex(regst[0]), this.r0Point.x, this.r0Point.y);
    ctx.fillText("r1: " + cpuElements.stdLogicNativeToHex(regst[1]), this.r1Point.x, this.r1Point.y);
    ctx.fillText("r2: " + cpuElements.stdLogicNativeToHex(regst[2]), this.r2Point.x, this.r2Point.y);
    ctx.fillText("r3: " + cpuElements.stdLogicNativeToHex(regst[3]), this.r3Point.x, this.r3Point.y);
    ctx.fillText("PC: " + stdlogVectorToString(circuit.pc_address,true), this.pcPoint.x, this.pcPoint.y);
    ctx.fillText("IR: " + stdlogVectorToString(circuit.ir_data,true), this.irPoint.x, this.irPoint.y);
    ctx.fillText("IRA: " + stdlogVectorToString(circuit.ira_data,true), this.iraPoint.x, this.iraPoint.y);
    ctx.fillText("MAR: " + stdlogVectorToString(circuit.address,true), this.marPoint.x, this.marPoint.y);
    let ci = cycleIndex(circuit.ringc_data);
    ctx.fillText("Cycle: " + decodeCycle(ci), this.cyclePoint.x, this.cyclePoint.y);
    let instr = decodeInstruction(ci, circuit.ir_data, circuit.ira_data);
    if(ci.err || ci.index < 6) {
      instrDescCache = instr;
    } else {
      instr = instrDescCache;
    }
    if(instr != null) ctx.fillText("=> " + instr, this.instrPoint.x, this.instrPoint.y);
    ctx.fillText("C,Z="+stdlogToString(circuit.carry_flag)+","+stdlogToString(circuit.zero_flag), this.czPoint.x, this.czPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(stdlogVectorToString(circuit.data, true), this.dataPoint.x+4, this.dataPoint.y-4);
    ctx.textAlign = "center";
    ctx.textBaseline = "top";
    ctx.fillText("DATA", this.dataPoint.x, this.dataPoint.y+4);

    ctx.textAlign = "left";
    ctx.textBaseline = "top";
    ctx.fillText(stdlogVectorToString(circuit.address, true), this.addrPoint.x+4, this.addrPoint.y+4);
    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("ADDRESS", this.addrPoint.x, this.addrPoint.y-4);

    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText(this.label, this.topLeft.x+this.width/2, this.topLeft.y+this.height-40);
  }
}

cpuElements.dataioElement = function(topLeft, datain, dataout, re, wr, databus) {
  this.topLeft = topLeft.copy();
  this.width = 70;
  this.height = 130;
  this.rePoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*2/5);
  this.wrPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*3/5);
  this.datainPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*1/5);
  this.dataoutPoint = new xypoint(this.topLeft.x, this.topLeft.y + this.height*4/5);
  this.databusPoint  = new xypoint(this.topLeft.x+this.width, this.topLeft.y + this.height/2);
  this.bgColor = "#F4A460";  // pješčano smeđa

  this.draw = function(ctx) {
    ctx.strokeStyle = "#000000";
    ctx.fillStyle = this.bgColor;
    ctx.beginPath();
    ctx.rect(this.topLeft.x, this.topLeft.y, this.width, this.height);
    ctx.fill();
    ctx.stroke();

    ctx.font="12px Georgia";
    ctx.textAlign = "right";
    ctx.textBaseline = "bottom";
    ctx.fillStyle = "#000000";
    ctx.fillText(cpuElements.vgVectorToString(datain, true), this.datainPoint.x-4, this.datainPoint.y-4);
    ctx.fillText(cpuElements.vgVectorToString(dataout, true), this.dataoutPoint.x-4, this.dataoutPoint.y-4);
    ctx.fillText(""+re.getValue(), this.rePoint.x-4, this.rePoint.y-4);
    ctx.fillText(""+wr.getValue(), this.wrPoint.x-4, this.wrPoint.y-4);

    ctx.textAlign = "left";
    ctx.textBaseline = "middle";
    ctx.fillText("data_in", this.datainPoint.x+4, this.datainPoint.y);
    ctx.fillText("data_out", this.dataoutPoint.x+4, this.dataoutPoint.y);
    ctx.fillText("re", this.rePoint.x+4, this.rePoint.y);
    ctx.fillText("wr", this.wrPoint.x+4, this.wrPoint.y);

    ctx.textAlign = "left";
    ctx.textBaseline = "bottom";
    ctx.fillText(cpuElements.vgVectorToString(databus, true), this.databusPoint.x+4, this.databusPoint.y-4);
    ctx.textAlign = "right";
    ctx.textBaseline = "middle";
    ctx.fillText("data", this.databusPoint.x-4, this.databusPoint.y);

    ctx.textAlign = "center";
    ctx.textBaseline = "bottom";
    ctx.fillText("dataIO", this.topLeft.x+this.width/2, this.topLeft.y+this.height-1);
  }
}

