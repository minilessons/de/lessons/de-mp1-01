/**
 * Simple simulation organizer.
 *
 * @returns {{init: initOrganizer}}
 * @constructor
 */
function SimulationOrganizer() {
    let swapComponentHolder = null;

    let nextBtn = null;
    let previousBtn = null;
    let btnNextName = "Dalje";
    let btnPreviousName = "Natrag";

    let instructions = null;
    let unpackedInstructions = [];
    let currentInstruction = 0;


    /**
     * Helper function to check if index is last for given array.
     * @param idx index to check
     * @param arr array to check index for
     * @returns {boolean} true if is
     */
    function isLastIndex(idx, arr) {
        return idx >= (arr.length - 1);
    }


    /**
     * Helper function for creating new swap component (simple div element).
     *
     * @returns {Element}
     */
    function createSwapComponent() {
        let comp = document.createElement("div");
        comp.setAttribute("style",
            "border-radius: 5px;" +
            "border: 2px solid #ff58ce;" +
            "background-color: #72BBBD;" +
            "padding-left: 4px;"
        );
        comp.id = "swapComponentHash";

        return comp;
    }


    /**
     * Initializes component for given ordered list.
     *
     * @param oList ordered list element
     */
    function initOrganizer(oList) {
        hideElement(oList);
        let testSection = oList;

        swapComponentHolder = createSwapComponent();

        instructions = Array.from(testSection.childNodes).filter((e) => {
            return e.tagName === "LI" || e.tagName === "li";
        });

        if (instructions.length <= 0) {
            throw new Error("I need at least one list element to make organizer.");
        }

        processInstructionElements(instructions);
        unpackedInstructions.forEach((instruction) => {
            swapComponentHolder.appendChild(instruction);
        });

        hideElements(unpackedInstructions);
        showCurrentInstruction();


        let btnBar = createBtnBar();


        // Add previous btn to div
        previousBtn = document.createElement("button");
        previousBtn.innerHTML = btnPreviousName;
        previousBtn.onclick = navigationOnClickEvent;
        btnBar.appendChild(previousBtn);

        // Add next button to div
        nextBtn = document.createElement("button");
        nextBtn.innerHTML = btnNextName;
        nextBtn.onclick = navigationOnClickEvent;
        btnBar.appendChild(nextBtn);

        swapComponentHolder.insertBefore(btnBar, swapComponentHolder.nextSibling);

        testSection.parentNode.insertBefore(swapComponentHolder, testSection);

        setupButtons();

    }


    /**
     * Helper function for creating button bar.
     *
     * @returns {Element}
     */
    function createBtnBar() {
        let btnBar = document.createElement("div");
        btnBar.setAttribute("align", "right");
        return btnBar;
    }


    /**
     * Transverse all instructions and unpacks them to a list of all swappable components.
     *
     * @param instructions list of <li> elements from ordered list (given at init)
     */
    function processInstructionElements(instructions) {
        instructions.forEach((ins) => {

            let nodes = Array.from(ins.childNodes).filter((e) => {
                return e.nodeType !== 3;
            });

            let title = nodes.filter((node) => {
                return (node.className === "swapTitle");
            });

            if (title.length === 0) {
                title = [document.createElement("div")];
            }

            let orderedLists = nodes.filter(node => {
                return (((node.tagName === "OL") || (node.tagName === "ol")) && (node.className === "swapSubSection"));
            });


            if (orderedLists.length === 1) {
                let textElements = Array.from(orderedLists[0].childNodes)
                    .filter((node) => {
                        return ((node.tagName === "LI") || (node.tagName === "li"));
                    })
                    .map((node) => {
                        let elHolder = document.createElement("div");
                        elHolder.innerHTML = node.innerHTML;
                        return elHolder;
                    });


                textElements.forEach((e) => {
                    let cloned = title[0].cloneNode(true);
                    let holder = createHolder();
                    holder.appendChild(cloned);
                    holder.appendChild(e);

                    unpackedInstructions.push(holder);
                });
            } else {
                let instructionTexts = nodes.filter((node) => {
                    return !(node.className === "swapTitle");
                });

                let textElement = document.createElement("div");
                instructionTexts.forEach((e) => {
                    textElement.appendChild(e);
                });

                let holder = createHolder();
                holder.appendChild(title[0]);
                holder.appendChild(textElement);

                unpackedInstructions.push(holder);
            }

        });

    }


    /**
     * Helper function to create container for title and text components in swap section.
     *
     * @returns {Element} simple div element
     */
    function createHolder() {
        let holder = document.createElement("div");
        holder.setAttribute("style", "height:150px; overflow-y:auto");

        return holder;
    }


    /**
     * Helper function to set up buttons availability.
     */
    function setupButtons() {
        if (unpackedInstructions.length === 1) {
            nextBtn.disabled = true;
        }

        previousBtn.disabled = true;
    }


    /**
     * Helper function to show HTML element.
     *
     * @param element element to show
     */
    function showElement(element) {
        showElements([element]);
    }


    /**
     * Helper function to show HTML list of elements.
     *
     * @param l list of elements
     */
    function showElements(l) {
        l.forEach((e) => {
            e.style.display = "block";
        });
    }


    /**
     * Helper function to hide HTML element.
     *
     * @param element element
     */
    function hideElement(element) {
        hideElements([element]);
    }


    /**
     * Helper function to hide list of HTML elements.
     *
     * @param l list of elements
     */
    function hideElements(l) {
        l.forEach((e) => {
            e.style.display = "none";
        });

    }


    /**
     * Helper function to hide current instruction.
     */
    function hideCurrentInstruction() {
        hideElement(unpackedInstructions[currentInstruction]);
    }


    /**
     * Helper function to show current instruction.
     */
    function showCurrentInstruction() {
        showElement(unpackedInstructions[currentInstruction]);
    }


    /**
     * Event callback for next and previous buttons.
     *
     * @param e event object
     */
    function navigationOnClickEvent(e) {
        if (e.target.innerHTML === btnNextName) {
            hideCurrentInstruction();
            currentInstruction += 1;
            showCurrentInstruction();

            if (isLastIndex(currentInstruction, unpackedInstructions)) {
                nextBtn.disabled = true;
            }

            previousBtn.disabled = false;
        } else if (e.target.innerHTML === btnPreviousName) {
            hideCurrentInstruction();
            currentInstruction -= 1;
            showCurrentInstruction();

            if (currentInstruction <= 0) {
                previousBtn.disabled = true;
            }

            nextBtn.disabled = false;
        }
    }


    return {
        init: initOrganizer
    }
}


let organizer = new SimulationOrganizer();
let swapSection = document.getElementById("swapSection1");
organizer.init(swapSection);