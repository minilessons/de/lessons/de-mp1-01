    // Expects that the action object has .execute() method.
    function ActiveRegion(x,y,w,h,action) {
      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;
      this.action = action;
      this.fire = function(info) {
        this.action.execute(info);
      };
      this.inside = function(x,y) {
        if(x < this.x || y < this.y) return false;
        if(x >= this.x+this.w || y >= this.y+this.h) return false;
        return true;
      }
    }
    // Collection of action regions. Offers:
    // .add(activeRegion) ... adds new action region in collection
    // .reset() ... clears list of registered active regions
    // .process(x,y): boolean ... executes registered active region for given point (x,y); returns true if such region exists; false otherwise.
    function ActiveRegions() {
      this.list = [];
      this.add = function(activeRegion) {
        this.list.push(activeRegion);
      }
      this.reset = function() {
        this.list = [];
      }
      this.process = function(x,y,info) {
        for(var i = 0; i < this.list.length; i++) {
          if(!this.list[i].inside(x,y)) continue;
          this.list[i].fire(info);
          return true;
        }
        return false;
      }
    }
    // Connects mouse handling with active regions for canvas wrapper.
    // Object canvasPaintObject must offer .canvas property which is JavaScript Canvas,
    // and .paint() method which should be automatically called to repaint canvas if
    // any active region is activated.
    // button: 0-left, 1-middle, 2-right
    function connectCanvasActiveRegion(canvasPaintObject, activeRegions) {
      canvasPaintObject.canvas.addEventListener('mousedown', function(evt) {
        var rect = canvasPaintObject.canvas.getBoundingClientRect();
        var pos = {x: evt.clientX - rect.left, y: evt.clientY - rect.top};
        if(activeRegions.process(pos.x, pos.y, {button: evt.button, ctrlKey: evt.ctrlKey, altKey: evt.altKey, shiftKey: evt.shiftKey})) {
          canvasPaintObject.paint();
        }
      }, false);
    }

