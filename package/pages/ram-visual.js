function RAMVisualContent(contentCanvasName, circuit) {
    let contentCanvas = document.getElementById(contentCanvasName);
    let contentContext = contentCanvas.getContext("2d");
    let contentElement = new cpuElements.ramContentElement(
      new xypoint(0, 0), circuit
    );
    function paint() {
      contentContext.clearRect(0, 0, contentCanvas.width, contentCanvas.height);
      contentElement.draw(contentContext);
    }
    circuit.operationListeners.push({execute: function() {
      paint();
    }});
    paint();
}

function RAMVisual(canvasName, as, rw, cs0, cs1, ds, asDriver, rwDriver, cs0Driver, cs1Driver, dsDriver, circuit, simenv) {
  this.canvas = document.getElementById(canvasName);
  this.context = this.canvas.getContext("2d");
  this.elements = [];
  this.currents = [];

  this.registerContentObserver = function(contentCanvasName) {
    let contentCanvas = document.getElementById(contentCanvasName);
    let contentContext = contentCanvas.getContext("2d");
    let contentElement = new cpuElements.ramContentElement(
      new xypoint(0, 0), circuit
    );
    function paint() {
      contentContext.clearRect(0, 0, contentCanvas.width, contentCanvas.height);
      contentElement.draw(contentContext);
    }
    circuit.operationListeners.push({execute: function() {
      paint();
    }});
    paint();
  }

  this.activeRegions = new ActiveRegions();
  connectCanvasActiveRegion(this, this.activeRegions);

  initElements(this);

  function initElements(me) {
    // Set floating topleft corner...
    var xoff = 305;
    var yoff = 65;

    var elems = [];
    var c = [];
    c.push(new flowValue()); // Zero flow

    me.elements = elems;
    me.currents = c;

    let ram = new cpuElements.ramElement(
      new xypoint(xoff, yoff), 
      dlsimul.engine.signalsToValueGetters(as),
      dlsimul.engine.signalToValueGetter(rw),
      dlsimul.engine.signalToValueGetter(cs0),
      dlsimul.engine.signalToValueGetter(cs1),
      dlsimul.engine.signalsToValueGetters(ds)
    );

    let dsb = new cpuElements.vectorSetterBoxBuffer(
      ram.dataPoint.copy().translate(-90, -30),
      'Z',
      simenv.createVectorSignalChangeSchedulers(ds, dsDriver, 1),
      dlsimul.std_logic_1164.simpleWithZValueToggler,
      ["D7", "D6", "D5", "D4", "D3", "D2", "D1", "D0"]
    );
    let asb = new cpuElements.vectorSetterBox(
      ram.addrPoint.copy().translate(-90, 30),
      dlsimul.engine.signalsToValueGetters(as),
      simenv.createVectorSignalChangeSchedulers(as, asDriver, 1),
      dlsimul.std_logic_1164.simpleValueToggler,
      ["A7", "A6", "A5", "A4", "A3", "A2", "A1", "A0"]
    );

    let rwb = new cpuElements.vectorSetterBox(
      ram.rwnPoint.copy().translate(-90, 0),
      dlsimul.engine.signalsToValueGetters([rw]),
      simenv.createVectorSignalChangeSchedulers([rw], [rwDriver], 1),
      dlsimul.std_logic_1164.simpleValueToggler,
      ["R/W'"]
    );
    rwb.labelPlacement = 1;

    let cs0b = new cpuElements.vectorSetterBox(
      ram.cs0Point.copy().translate(-90, 0),
      dlsimul.engine.signalsToValueGetters([cs0]),
      simenv.createVectorSignalChangeSchedulers([cs0], [cs0Driver], 1),
      dlsimul.std_logic_1164.simpleValueToggler,
      ["CS0"]
    );
    cs0b.labelPlacement = 1;

    let cs1b = new cpuElements.vectorSetterBox(
      ram.cs1Point.copy().translate(-90, 0),
      dlsimul.engine.signalsToValueGetters([cs1]),
      simenv.createVectorSignalChangeSchedulers([cs1], [cs1Driver], 1),
      dlsimul.std_logic_1164.simpleValueToggler,
      ["CS1"]
    );
    cs1b.labelPlacement = 1;

    dsb.registerActiveRegions(me.activeRegions);
    asb.registerActiveRegions(me.activeRegions);
    rwb.registerActiveRegions(me.activeRegions);
    cs0b.registerActiveRegions(me.activeRegions);
    cs1b.registerActiveRegions(me.activeRegions);

    elems.push(ram);
    elems.push(dsb);
    elems.push(asb);
    elems.push(rwb);
    elems.push(cs0b);
    elems.push(cs1b);

    elems.push(new wire(null, dsb.middleRight).advanceToX(ram.dataPoint.x).advanceToPoint(ram.dataPoint));
    elems.push(new wire(null, asb.middleRight).advanceToX(ram.addrPoint.x).advanceToPoint(ram.addrPoint));
    elems.push(new wire(null, rwb.middleRight).advanceToPoint(ram.rwnPoint));
    elems.push(new wire(null, cs0b.middleRight).advanceToPoint(ram.cs0Point));
    elems.push(new wire(null, cs1b.middleRight).advanceToPoint(ram.cs1Point));
  }

  this.paint = function() {
    //console.log("Repaint; animTime = " + animTime +", deltaTime = " + deltaTime);
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.textAlign = "start";
    this.context.textBaseline = "alphabetic";

    var ctx = this.context;
    this.elements.forEach(function(elem, index) {
      elem.draw(ctx, 0, 0);
    });
  }

  this.paint();
}

