function system1vis(canvasName, cp, res, cpDriver, resDriver, circuit, simenv) {
  this.canvas = document.getElementById(canvasName);
  this.context = this.canvas.getContext("2d");
  this.elements = [];
  this.currents = [];

  this.activeRegions = new ActiveRegions();
  connectCanvasActiveRegion(this, this.activeRegions);

  initElements(this);

  function initElements(me) {
    // Set floating topleft corner...
    var xoff = 130;
    var yoff = 30;

    var elems = [];

    me.elements = elems;

    let cpuComp = new cpuElements.cup1InfoElement(
      new xypoint(xoff, yoff),
      circuit.circuits[0]
    );
    elems.push(cpuComp);

    let resb = new cpuElements.vectorSetterBox(
      cpuComp.resPoint.copy().translate(-50, 0),
      dlsimul.engine.signalsToValueGetters([res]),
      simenv.createVectorSignalChangeSchedulers([res], [resDriver], 1),
      dlsimul.std_logic_1164.simpleValueToggler,
      ["RESET"]
    );
    resb.labelPlacement = 1;

    let cpb = new cpuElements.vectorSetterBox(
      cpuComp.cpPoint.copy().translate(-50, 0),
      dlsimul.engine.signalsToValueGetters([cp]),
      simenv.createVectorSignalChangeSchedulers([cp], [cpDriver], 1),
      dlsimul.std_logic_1164.simpleValueToggler,
      ["CP"]
    );
    cpb.labelPlacement = 1;

    cpb.registerActiveRegions(me.activeRegions);
    resb.registerActiveRegions(me.activeRegions);
    elems.push(cpb);
    elems.push(resb);

    let ram = new cpuElements.ramElement(
      cpuComp.topLeft.copy().translate(cpuComp.width+100,40), 
      dlsimul.engine.signalsToValueGetters(circuit.address),
      dlsimul.engine.signalToValueGetter(circuit.rw),
      dlsimul.engine.signalToValueGetter(circuit.mr),
      dlsimul.engine.signalToValueGetter(circuit.cs1),
      dlsimul.engine.signalsToValueGetters(circuit.data)
    );

    elems.push(ram);



/*

    let reg = new cpuElements.parallelRegister(
      new xypoint(xoff, yoff), 
      dlsimul.engine.signalsToValueGetters(ds),
      true,
      dlsimul.engine.signalToValueGetter(ce),
      dlsimul.engine.signalToValueGetter(cp),
      dlsimul.engine.signalsToValueGetters(qs),
      true,
      dlsimul.engine.signalToValueGetter(res)
    );

    let dsb = new cpuElements.vectorSetterBox(
      reg.dataPoint.copy().translate(-90, 0),
      dlsimul.engine.signalsToValueGetters(ds),
      simenv.createVectorSignalChangeSchedulers(ds, dsDriver, 1),
      dlsimul.std_logic_1164.simpleValueToggler,
      ["D7", "D6", "D5", "D4", "D3", "D2", "D1", "D0"]
    );

    let ceb = new cpuElements.vectorSetterBox(
      reg.cePoint.copy().translate(-90, 0),
      dlsimul.engine.signalsToValueGetters([ce]),
      simenv.createVectorSignalChangeSchedulers([ce], [ceDriver], 1),
      dlsimul.std_logic_1164.simpleValueToggler,
      ["CE"]
    );
    ceb.labelPlacement = 1;

    dsb.registerActiveRegions(me.activeRegions);
    ceb.registerActiveRegions(me.activeRegions);

    elems.push(reg);
    elems.push(dsb);
    elems.push(ceb);

    let w1 = new wire(null, reg.dataPoint); w1.advanceToPoint(dsb.middleRight);
    elems.push(w1);

    let w1b = new wire(null, reg.cePoint); w1b.advanceToPoint(ceb.middleRight);
    elems.push(w1b);

    let w1c = new wire(null, reg.cpPoint); w1c.advanceToPoint(cpb.middleRight);
    elems.push(w1c);

    let w2 = new wire(null, resb.middleRight); 
    w2.nextXY(reg.resPoint.x, resb.middleRight.y);
    w2.nextXY(reg.resPoint.x, reg.resPoint.y);
    elems.push(w2);

    let w3 = new wire(null, reg.oPoint); w3.advance(40, 0);
    elems.push(w3);*/
    elems.push(new wire(null, resb.middleRight).advanceToPoint(cpuComp.resPoint));
    elems.push(new wire(null, cpb.middleRight).advanceToPoint(cpuComp.cpPoint));

    elems.push(new wire(null, cpuComp.rwPoint).advance((ram.rwnPoint.x - cpuComp.rwPoint.x)/2,0).advanceToY(ram.rwnPoint.y).advanceToPoint(ram.rwnPoint));
    elems.push(new wire(null, cpuComp.mrPoint).advance((ram.cs0Point.x - cpuComp.mrPoint.x)/2,0).advanceToY(ram.cs0Point.y).advanceToPoint(ram.cs0Point));
    elems.push(new wire(null, ram.cs1Point).advance(-20,0));

    elems.push(new wire(null, cpuComp.dataPoint).advance(0,-25).advanceToX(ram.dataPoint.x).advanceToPoint(ram.dataPoint));
    elems.push(new wire(null, cpuComp.addrPoint).advance(0, 25).advanceToX(ram.addrPoint.x).advanceToPoint(ram.addrPoint));
  }

  this.paint = function() {
    //console.log("Repaint; animTime = " + animTime +", deltaTime = " + deltaTime);
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    this.context.textAlign = "start";
    this.context.textBaseline = "alphabetic";

    var ctx = this.context;
    this.elements.forEach(function(elem, index) {
      elem.draw(ctx, 0, 0);
    });
  }

  this.paint();
}

