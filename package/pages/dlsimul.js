var dlsimul = dlsimul || {};

if(dlsimul.namespaceDefined === undefined) {

  dlsimul.namespaceDefined = true;

  // start of dlsimul.std_logic_1164 namespace
  dlsimul.std_logic_1164 = new function() {

    this.std_error = function(message) {
      this.name = 'std_logic_1164_Error';
      this.message = message || 'std_logic_1164 error occured.';
      this.stack = (new Error()).stack;
    }

    this.std_error.prototype = Object.create(Error.prototype);
    this.std_error.prototype.constructor = this.std_error;

    this.values = [
     'U',  // Uninitialized
     'X',  // Forcing  Unknown
     '0',  // Forcing  0
     '1',  // Forcing  1
     'Z',  // High Impedance
     'W',  // Weak     Unknown
     'L',  // Weak     0
     'H',  // Weak     1
     '-'   // Don't care
    ];

    this.valueIndex = function(v) {
      let i = dlsimul.std_logic_1164.values.indexOf(v);
      if(i===-1) throw new dlsimul.std_logic_1164.std_error("'"+v+"' is not legal std_logic value.");
      return i;
    }

//    resolutionTable = [
//     [U,U,U,U,U,U,U,U,U],
//     [U,X,X,X,X,X,X,X,X],
//     [U,X,0,X,0,0,0,0,X],
//     [U,X,X,1,1,1,1,1,X],
//     [U,X,0,1,Z,W,L,H,X],
//     [U,X,0,1,W,W,W,W,X],
//     [U,X,0,1,L,W,L,W,X],
//     [U,X,0,1,H,W,W,H,X],
//     [U,X,X,X,X,X,X,X,X]];

    this.resolutionTable = [
     [0,0,0,0,0,0,0,0,0],
     [0,1,1,1,1,1,1,1,1],
     [0,1,2,1,2,2,2,2,1],
     [0,1,1,3,3,3,3,3,1],
     [0,1,2,3,4,5,6,7,1],
     [0,1,2,3,5,5,5,5,1],
     [0,1,2,3,6,5,6,5,1],
     [0,1,2,3,7,5,5,7,1],
     [0,1,1,1,1,1,1,1,1]];

    this.resolveValue = function(arr) {
      let res = 4; // 'Z'
      for(let i = 0; i < arr.length; i++) {
        res = dlsimul.std_logic_1164.resolutionTable[res][arr[i]];
      }
      return res;
    }

//    andTable = [
//     [U,U,0,U,U,U,0,U,U],
//     [U,X,0,X,X,X,0,X,X],
//     [0,0,0,0,0,0,0,0,0],
//     [U,X,0,1,X,X,0,1,X],
//     [U,X,0,X,X,X,0,X,X],
//     [U,X,0,X,X,X,0,X,X],
//     [0,0,0,0,0,0,0,0,0],
//     [U,X,0,1,X,X,0,1,X],
//     [U,X,0,X,X,X,0,X,X]

    this.andTable = [
    [0,0,2,0,0,0,2,0,0],
    [0,1,2,1,1,1,2,1,1],
    [2,2,2,2,2,2,2,2,2],
    [0,1,2,3,1,1,2,3,1],
    [0,1,2,1,1,1,2,1,1],
    [0,1,2,1,1,1,2,1,1],
    [2,2,2,2,2,2,2,2,2],
    [0,1,2,3,1,1,2,3,1],
    [0,1,2,1,1,1,2,1,1]];

//    orTable = [
//     [U,U,U,1,U,U,U,1,U],
//     [U,X,X,1,X,X,X,1,X],
//     [U,X,0,1,X,X,0,1,X],
//     [1,1,1,1,1,1,1,1,1],
//     [U,X,X,1,X,X,X,1,X],
//     [U,X,X,1,X,X,X,1,X],
//     [U,X,0,1,X,X,0,1,X],
//     [1,1,1,1,1,1,1,1,1],
//     [U,X,X,1,X,X,X,1,X]

    this.orTable = [
     [0,0,0,3,0,0,0,3,0],
     [0,1,1,3,1,1,1,3,1],
     [0,1,2,3,1,1,2,3,1],
     [3,3,3,3,3,3,3,3,3],
     [0,1,1,3,1,1,1,3,1],
     [0,1,1,3,1,1,1,3,1],
     [0,1,2,3,1,1,2,3,1],
     [3,3,3,3,3,3,3,3,3],
     [0,1,1,3,1,1,1,3,1]];

//    xorTable = [
//     [U,U,U,U,U,U,U,U,U],
//     [U,X,X,X,X,X,X,X,X],
//     [U,X,0,1,X,X,0,1,X],
//     [U,X,1,0,X,X,1,0,X],
//     [U,X,X,X,X,X,X,X,X],
//     [U,X,X,X,X,X,X,X,X],
//     [U,X,0,1,X,X,0,1,X],
//     [U,X,1,0,X,X,1,0,X],
//     [U,X,X,X,X,X,X,X,X]]

    this.xorTable = [
     [0,0,0,0,0,0,0,0,0],
     [0,1,1,1,1,1,1,1,1],
     [0,1,2,3,1,1,2,3,1],
     [0,1,3,2,1,1,3,2,1],
     [0,1,1,1,1,1,1,1,1],
     [0,1,1,1,1,1,1,1,1],
     [0,1,2,3,1,1,2,3,1],
     [0,1,3,2,1,1,3,2,1],
     [0,1,1,1,1,1,1,1,1]];

//    notTable = [U,X,1,0,X,X,1,0,X];

    this.notTable = [0,1,3,2,1,1,3,2,1];

//    toX01Table = [X,X,0,1,X,X,0,1,X];

    this.toX01Table = [1,1,2,3,1,1,2,3,1];

    this.functionAND = function(a,b) { return dlsimul.std_logic_1164.andTable[a][b]; }
    this.functionNAND = function(a,b) { return dlsimul.std_logic_1164.notTable[dlsimul.std_logic_1164.andTable[a][b]]; }
    this.functionOR = function(a,b) { return dlsimul.std_logic_1164.orTable[a][b]; }
    this.functionNOR = function(a,b) { return dlsimul.std_logic_1164.notTable[dlsimul.std_logic_1164.orTable[a][b]]; }
    this.functionXOR = function(a,b) { return dlsimul.std_logic_1164.xorTable[a][b]; }
    this.functionXNOR = function(a,b) { return dlsimul.std_logic_1164.notTable[dlsimul.std_logic_1164.xorTable[a][b]]; }
    this.functionNOT = function(a) { return dlsimul.std_logic_1164.notTable[a]; }
    this.toX01 = function(a) { return dlsimul.std_logic_1164.toX01Table[a]; }

    this.risingEdge = function(sigCurVal, sigLastVal) {
      return dlsimul.std_logic_1164.toX01(sigLastVal) === 2 && dlsimul.std_logic_1164.toX01(sigCurVal) === 3;
    }

    this.fallingEdge = function(sigCurVal, sigLastVal) {
      return dlsimul.std_logic_1164.toX01(sigLastVal) === 3 && dlsimul.std_logic_1164.toX01(sigCurVal) === 2;
    }

    this.simpleValueToggler = new function() {
      this.nextValue = function(v) {return v==='0' ? '1' : (v==='1' ? '0' : '0');};
      this.previousValue = function(v) {return v==='1' ? '0' : (v==='0' ? '1' : '0');};
    }

    this.simpleWithZValueToggler = new function() {
      this.nextValue = function(v) {return v==='0' ? '1' : (v==='1' ? 'Z' : '0');};
      this.previousValue = function(v) {return v==='0' ? 'Z' : (v==='Z' ? '1' : '0');};
    }

    return this;
  }
  // end of dlsimul.std_logic_1164 namespace

  // start of dlsimul.engine namespace
  dlsimul.engine = new function() {

    this.engine_error = function(message) {
      this.name = 'dlsimul.engine.engine_error';
      this.message = message || 'dlsimul.engine.engine_error occured.';
      this.stack = (new Error()).stack;
    }

    this.engine_error.prototype = Object.create(Error.prototype);
    this.engine_error.prototype.constructor = this.engine_error;

    this.createEngine = function() {
      let eventQueue = null;
      let eng = {signals: [], drivedValues: [], drivers: [], currentTime: 0, circuits: []};
      eng.createSignal = function(name, initValue) {
        let sig = {name: name, index: eng.signals.length, value: 0 /*'U'*/, initValue: initValue, lastValue: 0, listeners: [], activeEvent: false};
        eng.signals.push(sig);
        eng.drivedValues.push([]);
        return sig;
      };
      eng.createDriver = function(name) {
        let drv = {name: name, index: eng.drivers.length};
        eng.drivers.push(drv);
        return drv;
      };
      eng.registerCircuit = function(circuitType, name, impl) {
        let circ = {circuitType: circuitType, name: name, index: eng.circuits.length, impl: impl};
        eng.circuits.push(circ);
        return circ;
      };

      function enqueueSlot(time) {
        if(eventQueue==null || eventQueue.time > time) {
          let e = {time: time, next: eventQueue, previous: null, transactions: []};
          if(eventQueue!=null) eventQueue.previous = e;
          eventQueue = e;
          return eventQueue;
        }
        if(eventQueue.time == time) return eventQueue;
        let c = eventQueue;
        while(c.next != null && c.next.time < time) c = c.next;
        if(c.next != null && c.next.time == time) return c.next;
        let e = {time: time, next: c.next, previous: c, transactions: []};
        if(c.next!=null) c.next.previous = e;
        c.next = e;
        return e;
      }

      function scheduleTransaction(slot, driverIndex, signalIndex, value) {
        let t = slot.transactions.find(function(e) {return e.driverIndex==driverIndex && e.signalIndex==signalIndex;});
        if(t===undefined) {
          // This is transaction definition:
          t = {driverIndex: driverIndex, signalIndex: signalIndex, value: value};
          slot.transactions.push(t);
        } else {
          t.value = value;
        }
      }

      function setDrivedValue(transaction) {
        let t = eng.drivedValues[transaction.signalIndex].find(function(e) {return e.driverIndex==transaction.driverIndex;});
        if(t===undefined) {
          eng.drivedValues[transaction.signalIndex].push(transaction);
        } else {
          t.value = transaction.value;
        }
      }

      eng.scheduleDriverTransaction = function(driverIndex, signalIndex, value, time, cancelPending) {
        if(cancelPending) {
          for(let sl = eventQueue; sl != null; sl = sl.next) {
            let tind = sl.transactions.findIndex(function(e) {return e.driverIndex==driverIndex && e.signalIndex==signalIndex;});
            if(tind != -1) sl.transactions.splice(tind, 1);
          }
        }
        let slot = enqueueSlot(time);
        scheduleTransaction(slot, driverIndex, signalIndex, value);
      }

      function resolveDrivedValue(sigDrivers) {
        let arr = sigDrivers.map(function(e) {return e.value;});
        return dlsimul.std_logic_1164.resolveValue(arr);
      }

      eng.peekNextTime = function() { if(eventQueue==null) return null; return eventQueue.time; }

      eng.initSimulation = function() {
        eng.currentTime = 0;
        for(let i = 0; i < eng.drivedValues.length; i++) {
          eng.drivedValues[i] = [];
        }
        for(let i = 0; i < eng.signals.length; i++) {
          let s = eng.signals[i];
          s.value = s.initValue === undefined ? 0 /*U*/ : s.initValue;
          s.lastValue = s.value;
          s.activeEvent = false;
        }
        for(let i = 0; i < eng.circuits.length; i++) {
          eng.circuits.impl.execute(eng);
        }
      }

      eng.simulateNextStep = function() {
        if(eventQueue==null) return;
        let slot = eventQueue;
        eventQueue = eventQueue.next;
        if(eventQueue != null) eventQueue.previous = null;

        let candidateSignals = [];
        eng.currentTime = slot.time;
        console.log("Simulation time set at: " + eng.currentTime);
        for(let i = 0; i < slot.transactions.length; i++) {
          let t = slot.transactions[i];
          setDrivedValue(t);
          if(candidateSignals.indexOf(t.signalIndex) == -1) candidateSignals.push(t.signalIndex);
        }

        let candidateCircuits = [];
        let eventSignals = [];
        for(let i = 0; i < candidateSignals.length; i++) {
          let signalIndex = candidateSignals[i];
          let signal = eng.signals[signalIndex];
          let resValue = resolveDrivedValue(eng.drivedValues[signalIndex]);
          if(signal.value != resValue) {
            eventSignals.push(signal);
            signal.activeEvent = true;
            console.log("Updateing signal "+signal.name+" from "+dlsimul.std_logic_1164.values[signal.value]+" to " + dlsimul.std_logic_1164.values[resValue]);
            signal.lastValue = signal.value;
            signal.value = resValue;
            for(let j = 0; j < signal.listeners.length; j++) {
              let l = signal.listeners[j];
              if(candidateCircuits.indexOf(l) == -1) candidateCircuits.push(l);
            }
          }
        }

        for(let i = 0; i < candidateCircuits.length; i++) {
          candidateCircuits[i].execute(eng);
        }

        eventSignals.forEach(function(s) {s.activeEvent = false;});
      }

      eng.scheduleWaveForm = function(name, signal, arr) {
        let drv = eng.createDriver(name);
        for(let i = 0; i < arr.length; i++) {
          eng.scheduleDriverTransaction(drv.index, signal.index, arr[i][1], arr[i][0], false);
        }
      }

      eng.baseBinaryCirc = function(path, name, delay, inertial, inSignals, outSignal, binaryOperator, finalUnaryOperator) {
        let circName = path+"/"+name;
        let drv = eng.createDriver(circName+"/y");
        for(let i = 0; i < inSignals.length; i++) {
          inSignals[i].listeners.push(this);
        }
        this.execute = function() {
          if(inSignals.length<2) throw "Expected at least two signals!";
          let r = binaryOperator.call(null, inSignals[0].value, inSignals[1].value);
          for(let i = 2; i < inSignals.length; i++) {
            r = binaryOperator.call(null, r, inSignals[i].value);
          }
          if(!(finalUnaryOperator===undefined || finalUnaryOperator===null)) {
            r = finalUnaryOperator.call(null, r);
          }
          eng.scheduleDriverTransaction(drv.index, outSignal.index, r, eng.currentTime+delay, inertial);
        }
      }
      // multipleksor koji ima 's' selekcijskih bitova s kojima bira jedan od n=2^s podatkovnih ulaza, svaki podatkovni ulaz je k-bitni vektor (i izlaz je takav)
      // dataInputs: polje polja ulaznih signala; .length mora biti n, a [i].length mora biti k; [0] i [0][0] je ulaz najmanje težine
      // selectionInputs: polje selekcijskih signala; [0] je ulaz najveće težine
      // outSignals: polje izlaznih signala
      eng.mux_n_k_s = function(path, name, delay, inertial, dataInputs, selectionInputs, outSignals) {
        let s = selectionInputs.length;
        if(s<1) throw new dlsimul.engine.engine_error("mux_n_k_s: there must be at least one selection bit.");
        if(dataInputs.length != Math.pow(2, s)) new dlsimul.engine.engine_error("mux_n_k_s: mismatch between expected number of data inputs ("+Math.pow(2, s)+") and provided data inputs ("+dataInputs.length+").");
        for(let i = 0; i < dataInputs.length; i++) {
          if(dataInputs[i].length != outSignals.length) new dlsimul.engine.engine_error("mux_n_k_s: dataInputs["+i+"].length="+dataInputs[i].length+" but expected length based on outputs.length is "+outSignals.length+".");
        }
        for(let i = 0; i < selectionInputs.length; i++) {
          if(selectionInputs[i].listeners.indexOf(this)==-1) selectionInputs[i].listeners.push(this);
        }
        for(let i = 0; i < dataInputs.length; i++) {
          for(let j = 0; j < dataInputs[i].length; j++) {
            if(dataInputs[i][j].listeners.indexOf(this)==-1) dataInputs[i][j].listeners.push(this);
          }
        }
        let circName = path+"/"+name;
        let drvs = [];
        for(let i = 0; i < outSignals.length; i++) {
          drvs.push(eng.createDriver(circName+"/q"+i));
        }
        this.execute = function() {
          let resIndex = eng.binaryToIndex(selectionInputs);
          if(resIndex.err) {
            let r = dlsimul.std_logic_1164.valueIndex('X');
            for(let i = 0; i < outSignals.length; i++) {
              eng.scheduleDriverTransaction(drvs[i].index, outSignals[i].index, r, eng.currentTime+delay, inertial);
            }
          } else {
            for(let i = 0; i < outSignals.length; i++) {
              let r = dataInputs[resIndex.index][i].value;
              eng.scheduleDriverTransaction(drvs[i].index, outSignals[i].index, r, eng.currentTime+delay, inertial);
            }
          }
        }
      }

      function createSignalVector(nameKernel, n, initValue) {
        let res = [];
        for(let i = 0; i < n; i++) {
          res.push(eng.createSignal(nameKernel+i, dlsimul.std_logic_1164.valueIndex(initValue)));
        }
        return res;
      }

      eng.progCounter = function(path, name, delay, inertial, reset, cp, ce, pcsel, ira, address) {
        let circName = path+"/"+name;
        this.circuits = [];
        if(address.length<1) throw new dlsimul.engine.engine_error("progCounter: 'address' must have at least one element.");
        if(address.length!=ira.length) throw new dlsimul.engine.engine_error("progCounter: 'address' and 'ira' must have same number of elements: "+address.length+" vs. "+ira.length+".");
        let n = address.length;
        let dataSignals = createSignalVector(circName+"/dataSignal_", n, 'U');
        let bSignals = createSignalVector(circName+"/bSignal_", n, '0');
        bSignals[n-1].initValue = dlsimul.std_logic_1164.valueIndex('1');
        let cin = eng.createSignal(circName+"/cin", dlsimul.std_logic_1164.valueIndex('0'));
        let cout = eng.createSignal(circName+"/cout", dlsimul.std_logic_1164.valueIndex('U'));
        let sumSignals = createSignalVector(circName+"/sumSignal_", n, 'U');
        this.circuits.push(new eng.nBitRegisterCirc(circName, "reginst", delay, inertial, cp, ce, reset, dataSignals, address));
        this.circuits.push(new eng.nRCACirc(circName, "rcainst", delay, inertial, address, bSignals, cin, sumSignals, cout));
        this.circuits.push(new eng.mux_n_k_s(circName, "muxbinst", delay, inertial, [sumSignals,ira], [pcsel], dataSignals));
        this.sum_b_input = bSignals;
        this.sum_cin = cin;
        this.sum_cout = cout;
        this.sum_sum = sumSignals;
        this.reg_data = dataSignals;
      }

      eng.alu1 = function(path, name, delay, inertial, a, b, apass, alurep, binv, bsel, cin, alursel, z, cout, rez) {
        let circName = path+"/"+name;
        this.circuits = [];
        if(a.length<1) throw new dlsimul.engine.engine_error("alu1: 'a' must have at least one element.");
        if(a.length!=b.length) throw new dlsimul.engine.engine_error("alu1: 'a' and 'b' must have same number of elements: "+a.length+" vs. "+b.length+".");
        if(a.length!=rez.length) throw new dlsimul.engine.engine_error("alu1: 'a' and 'rez' must have same number of elements: "+a.length+" vs. "+rez.length+".");
        let n = a.length;
        let apassSignals = createSignalVector(circName+"/apassSignal_", n, 'U');
        let alurepSignals = createSignalVector(circName+"/arepSignal_", n, 'U');
        let buiSignals = createSignalVector(circName+"/buiSignal_", n, 'U');
        let bmuxSignals = createSignalVector(circName+"/bmuxSignal_", n, 'U');
        let aandbSignals = createSignalVector(circName+"/aandbSignal_", n, 'U');
        let sumSignals = createSignalVector(circName+"/sumSignal_", n, 'U');
        this.circuits.push(new eng.passCirc(circName, "passinst", delay, inertial, apass, a, apassSignals));
        this.circuits.push(new eng.replicateCirc(circName, "replinst", delay, inertial, alurep, alurepSignals));
        this.circuits.push(new eng.controlledInverterCirc(circName, "cinvinst", delay, inertial, binv, b, buiSignals));
        this.circuits.push(new eng.mux_n_k_s(circName, "muxbinst", delay, inertial, [alurepSignals,buiSignals], [bsel], bmuxSignals));
        this.circuits.push(new eng.andCircVector(circName, "andinst", delay, inertial, a, b, aandbSignals));
        this.circuits.push(new eng.mux_n_k_s(circName, "muxrezinst", delay, inertial, [sumSignals,aandbSignals], [alursel], rez));
        this.circuits.push(new eng.zeroTestCirc(circName, "zeroinst", delay, inertial, sumSignals, z));
        this.circuits.push(new eng.nRCACirc(circName, "rcainst", delay, inertial, apassSignals, bmuxSignals, cin, sumSignals, cout));
        this.pass_output = apassSignals;
        this.repl_output = alurepSignals;
        this.invOutput = buiSignals;
        this.mux1_output = bmuxSignals;
        this.rca_sum = sumSignals;
        this.and_output = aandbSignals;
      }

      // upravljivi prolaz: ovisno o jednom zajedničkom upravljačkom signalu controlSignal na outSignals propušta izravno signale iz inSignals ako je controlSignal==1 odnosno sve nule inače
      eng.passCirc = function(path, name, delay, inertial, controlSignal, inSignals, outSignals) {
        this.andCircuits = [];
        if(inSignals.length<1) throw new dlsimul.engine.engine_error("passCirc: inSignals must have at least one element.");
        if(inSignals.length!=outSignals.length) throw new dlsimul.engine.engine_error("passCirc: inSignals and outSignals must have same number of elements: "+inSignals.length+" vs. "+outSignals.length+".");
        for(let i = 0; i < inSignals.length; i++) {
          this.andCircuits.push(new eng.andCirc(path+"/"+name, "andinst_"+i, delay, inertial, [controlSignal,inSignals[i]], outSignals[i]));
        }
      }

      // controlSignal replicira preko buffera na sve signale iz outSignals
      eng.replicateCirc = function(path, name, delay, inertial, controlSignal, outSignals) {
        this.bufferCircuits = [];
        if(outSignals.length<1) throw new dlsimul.engine.engine_error("replicateCirc: outSignals must have at least one element.");
        for(let i = 0; i < outSignals.length; i++) {
          this.bufferCircuits.push(new eng.bufferCirc(path+"/"+name, "buffinst_"+i, delay, inertial, controlSignal, outSignals[i]));
        }
      }

      // upravljivi invertor: ovisno o jednom zajedničkom upravljačkom signalu controlSignal na outSignals propušta ili izravno signale iz inSignals ili njihove komplemente
      eng.controlledInverterCirc = function(path, name, delay, inertial, controlSignal, inSignals, outSignals) {
        this.xorCircuits = [];
        if(inSignals.length<1) throw new dlsimul.engine.engine_error("controlledInverterCirc: inSignals must have at least one element.");
        if(inSignals.length!=outSignals.length) throw new dlsimul.engine.engine_error("controlledInverterCirc: inSignals and outSignals must have same number of elements: "+inSignals.length+" vs. "+outSignals.length+".");
        for(let i = 0; i < inSignals.length; i++) {
          this.xorCircuits.push(new eng.xorCirc(path+"/"+name, "xorinst_"+i, delay, inertial, [controlSignal,inSignals[i]], outSignals[i]));
        }
      }

      // vektorsko logičko-I (bitwise)
      eng.andCircVector = function(path, name, delay, inertial, inSignalsA, inSignalsB, outSignals) {
        this.andCircuits = [];
        if(inSignalsA.length<1) throw new dlsimul.engine.engine_error("andCircVector: inSignalsA must have at least one element.");
        if(inSignalsA.length!=inSignalsB.length) throw new dlsimul.engine.engine_error("andCircVector: inSignalsA and inSignalsB must have same number of elements: "+inSignalsA.length+" vs. "+inSignalsB.length+".");
        if(inSignalsA.length!=outSignals.length) throw new dlsimul.engine.engine_error("andCircVector: inSignals and outSignals must have same number of elements: "+inSignalsA.length+" vs. "+outSignals.length+".");
        for(let i = 0; i < inSignalsA.length; i++) {
          this.andCircuits.push(new eng.andCirc(path+"/"+name, "andinst_"+i, delay, inertial, [inSignalsA[i],inSignalsB[i]], outSignals[i]));
        }
      }

      eng.andCirc = function(path, name, delay, inertial, inSignals, outSignal) {
        eng.baseBinaryCirc.call(this, path, name, delay, inertial, inSignals, outSignal, dlsimul.std_logic_1164.functionAND);
      }

      eng.orCirc = function(path, name, delay, inertial, inSignals, outSignal) {
        eng.baseBinaryCirc.call(this, path, name, delay, inertial, inSignals, outSignal, dlsimul.std_logic_1164.functionOR);
      }

      eng.nandCirc = function(path, name, delay, inertial, inSignals, outSignal) {
        eng.baseBinaryCirc.call(this, path, name, delay, inertial, inSignals, outSignal, dlsimul.std_logic_1164.functionAND, dlsimul.std_logic_1164.functionNOT);
      }

      eng.norCirc = function(path, name, delay, inertial, inSignals, outSignal) {
        eng.baseBinaryCirc.call(this, path, name, delay, inertial, inSignals, outSignal, dlsimul.std_logic_1164.functionOR, dlsimul.std_logic_1164.functionNOT);
      }

      eng.xorCirc = function(path, name, delay, inertial, inSignals, outSignal) {
        eng.baseBinaryCirc.call(this, path, name, delay, inertial, inSignals, outSignal, dlsimul.std_logic_1164.functionXOR);
      }

      eng.xnorCirc = function(path, name, delay, inertial, inSignals, outSignal) {
        eng.baseBinaryCirc.call(this, path, name, delay, inertial, inSignals, outSignal, dlsimul.std_logic_1164.functionXNOR);
      }

      eng.zeroTestCirc = function(path, name, delay, inertial, inSignals, outSignal) {
        if(inSignals.length<1) throw new dlsimul.engine.engine_error("zeroTestCirc: inSignals must have at least one element.");
        let circName = path+"/"+name;
        let drv = eng.createDriver(circName+"/y");
        for(let i = 0; i < inSignals.length; i++) {
          inSignals[i].listeners.push(this);
        }
        this.execute = function() {
          let zero = true;
          let err = false;
          for(let i = 0; i < inSignals.length; i++) {
            let v = dlsimul.std_logic_1164.toX01(inSignals[i].value);
            if(dlsimul.std_logic_1164.values[v]=='0') continue;
            if(dlsimul.std_logic_1164.values[v]=='1') { zero = false; continue; }
            err = true;
          }
          if(err) {
            let r = dlsimul.std_logic_1164.valueIndex('X');
            eng.scheduleDriverTransaction(drv.index, outSignal.index, r, eng.currentTime+delay, inertial);
          } else {
            let r = dlsimul.std_logic_1164.valueIndex(zero ? '1' : '0');
            eng.scheduleDriverTransaction(drv.index, outSignal.index, r, eng.currentTime+delay, inertial);
          }
        }
      }

      // d flip-flop sa sinkronim set/reset (set je većeg prioriteta) koji djeluju na '1' i clock-enable (također na '1'); ako je bilo koji
      // od njih nepotreban, može se predati null; isto vrijedi i za qSignal i qnSignal, ali greška je ako su oba null!
      eng.dffsynsrCirc = function(path, name, delay, inertial, cpSignal, dSignal, setSignal, resetSignal, ceSignal, qSignal, qnSignal) {
        if(qSignal==null && qnSignal==null) throw new dlsimul.engine.engine_error("dffsynsrCirc: both q and qn can not be null!");
        let circName = path+"/"+name;
        let drvq = eng.createDriver(circName+"/q");
        let drvqn = eng.createDriver(circName+"/qn");
        cpSignal.listeners.push(this);
        dSignal.listeners.push(this);
        if(setSignal!=null) setSignal.listeners.push(this);
        if(resetSignal!=null) resetSignal.listeners.push(this);
        if(ceSignal!=null) ceSignal.listeners.push(this);
        let state = dlsimul.std_logic_1164.valueIndex('U');
        let ONE = dlsimul.std_logic_1164.valueIndex('1');
        let ZERO = dlsimul.std_logic_1164.valueIndex('0');
        this.getState = function() { return state; }
        this.execute = function() {
          if(cpSignal.activeEvent && dlsimul.std_logic_1164.risingEdge(cpSignal.value, cpSignal.lastValue)) {
            if(setSignal!=null && setSignal.value === ONE) {
              state = ONE;
              if(qSignal!=null) eng.scheduleDriverTransaction(drvq.index, qSignal.index, state, eng.currentTime+delay, inertial);
              if(qnSignal!=null) eng.scheduleDriverTransaction(drvqn.index, qnSignal.index, dlsimul.std_logic_1164.functionNOT(state), eng.currentTime+delay, inertial);
            } else if(resetSignal!=null && resetSignal.value === ONE) {
              state = ZERO;
              if(qSignal!=null) eng.scheduleDriverTransaction(drvq.index, qSignal.index, state, eng.currentTime+delay, inertial);
              if(qnSignal!=null) eng.scheduleDriverTransaction(drvqn.index, qnSignal.index, dlsimul.std_logic_1164.functionNOT(state), eng.currentTime+delay, inertial);
            } else if(ceSignal==null || ceSignal.value===ONE) {
              state = dSignal.value;
              if(qSignal!=null) eng.scheduleDriverTransaction(drvq.index, qSignal.index, state, eng.currentTime+delay, inertial);
              if(qnSignal!=null) eng.scheduleDriverTransaction(drvqn.index, qnSignal.index, dlsimul.std_logic_1164.functionNOT(state), eng.currentTime+delay, inertial);
              if(qSignal!=null) console.log(" ... Scheduling "+qSignal.name+" to change to "+dlsimul.std_logic_1164.values[state]+" at "+(eng.currentTime+delay));
            }
          }
        }
      }

      eng.ramCirc = function(path, name, delay, inertial, addrSignals, rwnSignal, cs0Signal, cs1Signal, dataSignals) {
        let dn = dataSignals.length;
        let an = addrSignals.length;
        if(dn<1) throw new dlsimul.engine.engine_error("ramCirc: data word length must be at least 1!");
        if(an<1) throw new dlsimul.engine.engine_error("ramCirc: address word length must be at least 1!");
        if(an>10) throw new dlsimul.engine.engine_error("ramCirc: address word of length "+an+" are intentionally not supported!");
        let aspace = Math.pow(2, an);
        let circName = path+"/"+name;
        let drvs = [];
        for(let i = 0; i < dn; i++) {
          drvs.push(eng.createDriver(circName+"/d"+i));
          dataSignals[i].listeners.push(this);
        }
        cs0Signal.listeners.push(this);
        cs1Signal.listeners.push(this);
        rwnSignal.listeners.push(this);
        for(let i = 0; i < an; i++) {
          addrSignals[i].listeners.push(this);
        }
        let ONE = dlsimul.std_logic_1164.valueIndex('1');
        let ZERO = dlsimul.std_logic_1164.valueIndex('0');
        let UNDEF = dlsimul.std_logic_1164.valueIndex('U');
        let ZVAL = dlsimul.std_logic_1164.valueIndex('Z');
        this.lastOperation = -1;  // -1: none; 0: write; 1: read
        this.lastOperationAddress = 0;
        this.operationListeners = [];
        this.addressBits = an;
        this.dataBits = dn;
        this.content = [];
        for(let wi = 0; wi < aspace; wi++) {
          let word = [];
          for(let i = 0; i < dn; i++) word.push(ZERO);
          this.content.push(word);
        }
        this.execute = function() {
          let resIndex = eng.binaryToIndex(addrSignals);
          if(cs0Signal.value != ONE || cs1Signal.value != ONE || (rwnSignal.value == ONE && rwnSignal.value == ZERO)) { // memory is not enabled or r/w is invalid...
            let shouldNotify = this.lastOperation != -1;
            this.lastOperation = -1;
            this.lastOperationAddress = 0;
            for(let i = 0; i < dn; i++) {
              eng.scheduleDriverTransaction(drvs[i].index, dataSignals[i].index, ZVAL, eng.currentTime+delay, inertial);
            }
            if(shouldNotify) for(let i = 0; i < this.operationListeners.length; i++) this.operationListeners[i].execute();
          } else {
            if(rwnSignal.value == ZERO) { // write from processor into memory
              let shouldNotify = false;
              if(resIndex.err && this.lastOperation != -1) { this.lastOperation = -1; this.lastOperationAddress = 0; shouldNotify = true; }
              else if(!resIndex.err && (this.lastOperation != 0 || this.lastOperationAddress != resIndex.index)) { this.lastOperation = 0; this.lastOperationAddress = resIndex.index; shouldNotify = true; }
              for(let i = 0; i < dn; i++) {
                if(!resIndex.err) this.content[resIndex.index][i] = dataSignals[i].value;
                eng.scheduleDriverTransaction(drvs[i].index, dataSignals[i].index, ZVAL, eng.currentTime+delay, inertial);
              }
              if(shouldNotify) for(let i = 0; i < this.operationListeners.length; i++) this.operationListeners[i].execute();
            } else {
              let shouldNotify = false;
              if(resIndex.err && this.lastOperation != -1) { this.lastOperation = -1; this.lastOperationAddress = 0; shouldNotify = true; }
              else if(!resIndex.err && (this.lastOperation != 1 || this.lastOperationAddress != resIndex.index)) { this.lastOperation = 1; this.lastOperationAddress = resIndex.index; shouldNotify = true; }
              for(let i = 0; i < dn; i++) {
                eng.scheduleDriverTransaction(drvs[i].index, dataSignals[i].index, resIndex.err ? UNDEF : this.content[resIndex.index][i], eng.currentTime+delay, inertial);
              }
              if(shouldNotify) for(let i = 0; i < this.operationListeners.length; i++) this.operationListeners[i].execute();
            }
          }
        };
        // data must be array of {0,1} integers! Can be used only before simulation is started.
        this.setContentWord = function(index, data) {
          if(index < 0 || index >= aspace) throw new dlsimul.engine.engine_error("ramCirc: address "+index+" is invalid!");
          if(data.length != dn) throw new dlsimul.engine.engine_error("ramCirc: data word length is invalid (expected "+dn+", got "+data.length+").");
          for(let i = 0; i < dn; i++) {
            if(data[i] != 0 && data[i] != 1) throw new dlsimul.engine.engine_error("ramCirc: data bit has invalid value. Allowed: {0, 1}, got "+data[i]+".");
          }
          for(let i = 0; i < dn; i++) {
            this.content[index][i] = data[i]==0 ? ZERO : ONE;
          }
        }
      }

      // n-bitni paralelni registar s CE i sinkronim resetom.
      eng.nBitRegisterCirc = function(path, name, delay, inertial, cpSignal, ceSignal, resetSignal, dataSignals, qSignals) {
        this.ffCircuits = [];
        if(dataSignals.length<1) throw new dlsimul.engine.engine_error("nBitRegisterCirc: dataSignals must have at least one element.");
        if(dataSignals.length!=qSignals.length) throw new dlsimul.engine.engine_error("nBitRegisterCirc: dataSignals and qSignals must have same number of elements: "+dataSignals.length+" vs. "+qSignals.length+".");
        for(let i = 0; i < dataSignals.length; i++) {
          this.ffCircuits.push(new eng.dffsynsrCirc(path+"/"+name, "ff_"+i, delay, inertial, cpSignal, dataSignals[i], null, resetSignal, ceSignal, qSignals[i], null));
        }
        this.getState = function() {
          let st = [];
          for(let i = 0; i < this.ffCircuits.length; i++) st.push(this.ffCircuits[i].getState());
          return st;
        }
      }

      // n-bitno prstenasto brojilo s CE i sinkronim resetom.
      eng.nBitRingCounterCirc = function(path, name, delay, inertial, cpSignal, ceSignal, resetSignal, qSignals) {
        this.ffCircuits = [];
        if(qSignals.length<1) throw new dlsimul.engine.engine_error("nBitRingCounterCirc: qSignals must have at least one element.");
        for(let i = 0; i < qSignals.length; i++) {
          this.ffCircuits.push(new eng.dffsynsrCirc(path+"/"+name, "ff_"+i, delay, inertial, cpSignal, qSignals[(i-1+qSignals.length)%qSignals.length], i===0 ? resetSignal : null, i===0 ? null : resetSignal, ceSignal, qSignals[i], null));
        }
        this.getState = function() {
          let st = [];
          for(let i = 0; i < this.ffCircuits.length; i++) st.push(this.ffCircuits[i].getState());
          return st;
        }
      }

      // half-adder
      eng.halfAdderCirc = function(path, name, delay, inertial, aiSignal, biSignal, siSignal, ciSignal) {
        this.circuits = [];
        this.circuits.push(new eng.xorCirc(path+"/"+name, "si", delay, inertial, [aiSignal, biSignal], siSignal));
        this.circuits.push(new eng.andCirc(path+"/"+name, "ci", delay, inertial, [aiSignal, biSignal], ciSignal));
      }

      // full-adder
      // genSignal, propSignal mogu biti null; ako jesu, za interni generate i propagate će se koristiti interni signali; inače će se koristiti predani signali
      eng.fullAdderCirc = function(path, name, delay, inertial, aiSignal, biSignal, cinSignal, siSignal, coutSignal, genSignal, propSignal) {
        this.circuits = [];
        let intsi = eng.createSignal(path+"/"+name+"/intsi", dlsimul.std_logic_1164.valueIndex('U'));
        let intcia = genSignal != null ? genSignal : eng.createSignal(path+"/"+name+"/intcia", dlsimul.std_logic_1164.valueIndex('U'));
        let intcib = propSignal != null ? propSignal : eng.createSignal(path+"/"+name+"/intcib", dlsimul.std_logic_1164.valueIndex('U'));
        this.circuits.push(new eng.halfAdderCirc(path+"/"+name, "ha_1", delay, inertial, aiSignal, biSignal, intsi, intcia));
        this.circuits.push(new eng.halfAdderCirc(path+"/"+name, "ha_2", delay, inertial, intsi, cinSignal, siSignal, intcib));
        this.circuits.push(new eng.orCirc(path+"/"+name, "orinst", delay, inertial, [intcia, intcib], coutSignal));
      }

      // n-bit Ripple Carry Adder; treats array element at position 0 as MSB
      eng.nRCACirc = function(path, name, delay, inertial, aiSignals, biSignals, cinSignal, siSignals, coutSignal) {
        if(aiSignals.length<1) throw new dlsimul.engine.engine_error("nRCACirc: aiSignals must have at least one element.");
        if(aiSignals.length!=biSignals.length) throw new dlsimul.engine.engine_error("nRCACirc: aiSignals and biSignals must have same number of elements: "+aiSignals.length+" vs. "+biSignals.length+".");
        if(aiSignals.length!=siSignals.length) throw new dlsimul.engine.engine_error("nRCACirc: aiSignals and siSignals must have same number of elements: "+aiSignals.length+" vs. "+siSignals.length+".");
        this.circuits = [];
        this.intCarrys = [];
        for(let i = 0; i < aiSignals.length-1; i++) {
          this.intCarrys.push(eng.createSignal(path+"/"+name+"/cint_"+i, dlsimul.std_logic_1164.valueIndex('U')));
        }
        for(let i = 0; i < aiSignals.length; i++) {
          if(i == aiSignals.length-1) {
            this.circuits.push(new eng.fullAdderCirc(path+"/"+name, "fa_"+i, delay, inertial, aiSignals[i], biSignals[i], cinSignal, siSignals[i], this.intCarrys[i-1], null, null));
          } else if(i == 0) {
            this.circuits.push(new eng.fullAdderCirc(path+"/"+name, "fa_"+i, delay, inertial, aiSignals[i], biSignals[i], this.intCarrys[i], siSignals[i], coutSignal, null, null));
          } else {
            this.circuits.push(new eng.fullAdderCirc(path+"/"+name, "fa_"+i, delay, inertial, aiSignals[i], biSignals[i], this.intCarrys[i], siSignals[i], this.intCarrys[i-1], null, null));
          }
        }
      }

      eng.notCirc = function(path, name, delay, inertial, inSignal, outSignal) {
        let circName = path+"/"+name;
        let drv = eng.createDriver(circName+"/y");
        inSignal.listeners.push(this);
        this.execute = function() {
          let r = dlsimul.std_logic_1164.functionNOT(inSignal.value);
          eng.scheduleDriverTransaction(drv.index, outSignal.index, r, eng.currentTime+delay, inertial);
        }
      }

      eng.bufferCirc = function(path, name, delay, inertial, inSignal, outSignal) {
        let circName = path+"/"+name;
        let drv = eng.createDriver(circName+"/y");
        inSignal.listeners.push(this);
        this.execute = function() {
          let r = inSignal.value;
          eng.scheduleDriverTransaction(drv.index, outSignal.index, r, eng.currentTime+delay, inertial);
        }
      }

      eng.bufferWithOECirc = function(path, name, delay, inertial, inSignal, oeSignal, outSignal) {
        this.enabledWithLow = false;
        let circName = path+"/"+name;
        let drv = eng.createDriver(circName+"/y");
        inSignal.listeners.push(this);
        oeSignal.listeners.push(this);
        let ONE = dlsimul.std_logic_1164.valueIndex('1');
        let ZERO = dlsimul.std_logic_1164.valueIndex('0');
        let ZVAL = dlsimul.std_logic_1164.valueIndex('Z');
        this.execute = function() {
          let r = inSignal.value;
          let oe = !this.enabledWithLow ? oeSignal.value === ONE : oeSignal.value === ZERO;
          eng.scheduleDriverTransaction(drv.index, outSignal.index, oe ? r : ZVAL, eng.currentTime+delay, inertial);
        }
      }

      // input-output buffer
      eng.ioBufferCirc = function(path, name, delay, inertial, dioSignals, re, wr, diSignals, doSignals, reEnabledWithLow, wrEnabledWithLow) {
        if(dioSignals.length<1) throw new dlsimul.engine.engine_error("ioBufferCirc: dioSignals must have at least one element.");
        if(diSignals.length!=dioSignals.length) throw new dlsimul.engine.engine_error("ioBufferCirc: diSignals and dioSignals must have same number of elements: "+diSignals.length+" vs. "+dioSignals.length+".");
        if(doSignals.length!=dioSignals.length) throw new dlsimul.engine.engine_error("ioBufferCirc: doSignals and dioSignals must have same number of elements: "+doSignals.length+" vs. "+dioSignals.length+".");
        this.circuits = [];
        for(let i = 0; i < dioSignals.length; i++) {
          let b1 = new eng.bufferWithOECirc(path+"/"+name, "din_"+i, delay, inertial, dioSignals[i], re, diSignals[i]);
          let b2 = new eng.bufferWithOECirc(path+"/"+name, "don_"+i, delay, inertial, doSignals[i], wr, dioSignals[i]);
          b1.enabledWithLow = reEnabledWithLow;
          b2.enabledWithLow = wrEnabledWithLow;
          this.circuits.push(b1);
          this.circuits.push(b2);
        }
      }

      // binarni dekoder s ulazom za omogućavanje; ulaz za omogućavanje može biti null - tada je stalno omogućen.
      eng.binDecoderCirc = function(path, name, delay, inertial, addrSignals, eSignal, outSignals) {
        if(addrSignals.length<1) throw new dlsimul.engine.engine_error("binDecoderCirc: addrSignals must have at least one element.");
        let n = Math.pow(2, addrSignals.length);
        if(outSignals.length!=n) throw new dlsimul.engine.engine_error("binDecoderCirc: outSignals must have  "+n+" elements since address size is "+addrSignals.length+"; found "+outSignals.length+".");
        let circName = path+"/"+name;
        let drvs = [];
        for(let i = 0; i < n; i++) {
          drvs.push(eng.createDriver(circName+"/y"+i));
        }
        for(let i = 0; i < addrSignals.length; i++) {
          addrSignals[i].listeners.push(this);
        }
        if(eSignal != null) eSignal.listeners.push(this);
        let ONE = dlsimul.std_logic_1164.valueIndex('1');
        let ZERO = dlsimul.std_logic_1164.valueIndex('0');
        let UVAL = dlsimul.std_logic_1164.valueIndex('U');
        this.execute = function() {
          let r = eSignal==null ? ONE : eSignal.value;
          let resIndex = eng.binaryToIndex(addrSignals);
          let oe = r === ONE;
          for(let i = 0; i < n; i++) {
            eng.scheduleDriverTransaction(drvs[i].index, outSignals[i].index, oe ? (resIndex.err ? UVAL : (resIndex.index==i ? ONE : ZERO)) : ZERO, eng.currentTime+delay, inertial);
          }
        }
      }

      // utility funkcija za konverziju binarnog uzorka (vektor std_logic_1164 signala; pozicija 0 je MSB) u cijeli broj
      eng.binaryToIndex = function(signals) {
        let index = 0;
        let err = false;
        for(let i = 0; i < signals.length; i++) {
          index *= 2;
          let v = dlsimul.std_logic_1164.toX01(signals[i].value);
          if(dlsimul.std_logic_1164.values[v]=='0') continue;
          if(dlsimul.std_logic_1164.values[v]=='1') { index += 1; continue; }
          err = true;
          break;
        }
        return {err: err, index: index};
      }

      // register-bank; one-input (C), two-outputs (A,B) with WE (write-enable)
      eng.registerBankCirc = function(path, name, delay, inertial, aaddrSignals, baddrSignals, caddrSignals, aSignals, bSignals, cSignals, cpSignal, weSignal, resetSignal) {
        this.circuits = [];
        let k = aaddrSignals.length;
        if(k<1) throw new dlsimul.engine.engine_error("registerBankCirc: aaddrSignals must have at least one element.");
        if(baddrSignals.length!=k) throw new dlsimul.engine.engine_error("registerBankCirc: aaddrSignals and baddrSignals must have same number of elements.");
        if(caddrSignals.length!=k) throw new dlsimul.engine.engine_error("registerBankCirc: aaddrSignals and caddrSignals must have same number of elements.");
        let n = aSignals.length;
        if(n<1) throw new dlsimul.engine.engine_error("registerBankCirc: aSignals must have at least one element.");
        if(bSignals.length!=n) throw new dlsimul.engine.engine_error("registerBankCirc: aSignals and bSignals must have same number of elements.");
        if(cSignals.length!=n) throw new dlsimul.engine.engine_error("registerBankCirc: aSignals and cSignals must have same number of elements.");
        let regsn = Math.pow(2, k);
        let ces = [];
        for(let i = 0; i < regsn; i++) {
          ces.push(eng.createSignal(path+"/"+name+"/ce_"+i, dlsimul.std_logic_1164.valueIndex('U')));
        }
        let outSigs = [];
        for(let i = 0; i < regsn; i++) {
          let qsigs = [];
          for(let j = 0; j < n; j++) {
            qsigs.push(eng.createSignal(path+"/"+name+"/q_r"+i+"_"+j, dlsimul.std_logic_1164.valueIndex('U')));
          }
          outSigs.push(qsigs);
        }
        let regs = [];
        this.circuits.push(new eng.binDecoderCirc(path+"/"+name, "dek", delay, inertial, caddrSignals, weSignal, ces));
        for(let i = 0; i < regsn; i++) {
          let reg = new eng.nBitRegisterCirc(path+"/"+name, "reg_"+i, delay, inertial, cpSignal, ces[i], resetSignal, cSignals, outSigs[i]);
          this.circuits.push(reg);
          regs.push(reg);
        }
        this.circuits.push(new eng.mux_n_k_s(path+"/"+name, "mux_0", delay, inertial, outSigs, aaddrSignals, aSignals));
        this.circuits.push(new eng.mux_n_k_s(path+"/"+name, "mux_1", delay, inertial, outSigs, baddrSignals, bSignals));
        this.dek_outputs = ces;
        this.reg_outputs = outSigs;
        this.getState = function() {
          let st = [];
          for(let i = 0; i < regs.length; i++) st.push(regs[i].getState());
          return st;
        }
        let me = this;
        this.contentInfo = {
          getState: function() { return me.getState(); },
          registersCount: regs.length,
          wordWidth: regs[0].ffCircuits.length
        };
      }

      return eng;
    }

    return this;
  }

  dlsimul.engine.valueToSignalSetter = function(s) {
    return {setValue: function(v) {s.value = dlsimul.std_logic_1164.valueIndex(v);}};
  }
  dlsimul.engine.signalToValueGetter = function(s) {
    return {getValue: function() {return dlsimul.std_logic_1164.values[s.value];}};
  }
  dlsimul.engine.signalsToValueGetters = function(ss) {
    let vg = [];
    for(let i = 0; i < ss.length; i++) {
      vg.push(dlsimul.engine.signalToValueGetter(ss[i]));
    }
    return vg;
  }

  // end of dlsimul.engine namespace
}

