function dlsimulIncremental(timerObject, engine) {
  let me = this;
  var startTime = +new Date();

  this.engine = engine;

  this.paintComponents = [];

  this.timeSinceStart = function() {
    return (+new Date()) - startTime;
  }

  this.initialize = function() {
    startTime = +new Date();
  }

  this.signalChangeScheduler = function(signal, driver, delay) {
    return {
      setValue: function(v) {
        let now = me.timeSinceStart();
        console.log("scheduleSignalChange: now is " + now);
        engine.scheduleDriverTransaction(driver.index, signal.index, dlsimul.std_logic_1164.valueIndex(v), now+delay, false);
        console.log("Scheduling driver " + driver.name + " for " + signal.name + " to set " + dlsimul.std_logic_1164.valueIndex(v) + " at " + (now+delay));
        me.ping();
      }
    };
  }
  this.voidSignalChangeScheduler = function(signal, driver, delay) {
    return {
      setValue: function(v) {
      }
    };
  }

  this.createVectorSignalChangeSchedulers = function(signals, drivers, delay) {
    let arr = [];
    for(let i = 0; i < signals.length; i++) {
      if(drivers[i] == null) {
        arr.push(this.voidSignalChangeScheduler(signals[i], drivers[i], delay));
      } else {
        arr.push(this.signalChangeScheduler(signals[i], drivers[i], delay));
      }
    }
    return arr;
  }

  var simulationTimerID = null;
  var simulationTimerTargetedAt = null;

  this.paintRegisteredComponents = function() {
    for(let i = 0; i < me.paintComponents.length; i++) {
      me.paintComponents[i].paint();
    }
  }

  var paintNotCalled = true;

  this.ping = function() {
    let nextEventTime = engine.peekNextTime();
    if(nextEventTime==null) { if(paintNotCalled) { paintNotCalled=false; me.paintRegisteredComponents(); } return; }
    if(simulationTimerID==null || nextEventTime < simulationTimerTargetedAt) {
      if(simulationTimerID!=null) { simulationTimerID = null; timerObject.clearTimeout(simulationTimerID); }
      simulationTimerTargetedAt = nextEventTime;
      let nowTime = +new Date();
      let expectedSimTime = nowTime - startTime;
      simulationTimerID = timerObject.setTimeout(me.simulationTimerFunction, Math.max(0, nextEventTime - expectedSimTime));
    } else {
      if(paintNotCalled) { paintNotCalled=false; me.paintRegisteredComponents(); }
    }
  }
  this.simulationTimerFunction = function() {
    simulationTimerID = null;
    simulationTimerTargetedAt = null;
    let nowTime = +new Date();
    let expectedSimTime = nowTime - startTime;
    console.log("simulationTimerFunction triggered for t="+expectedSimTime);
    let nextEventTime = null;
    while(true) {
      nextEventTime = engine.peekNextTime();
      if(nextEventTime==null) {
        console.log("Simulator: no more events..."); 
        break;
      }
      if(nextEventTime > expectedSimTime+20) {
        console.log("Simulator: nextEventTime is scheduled for "+nextEventTime+", we are at "+expectedSimTime+". Going to sleep."); 
        break;
      }
      console.log("Simulator: simulating step at time="+nextEventTime); 
      engine.simulateNextStep();
    }
    me.paintRegisteredComponents();
    if(nextEventTime==null) return;
    simulationTimerID = timerObject.setTimeout(me.simulationTimerFunction, nextEventTime - expectedSimTime);
    simulationTimerTargetedAt = nextEventTime;
  }
}

