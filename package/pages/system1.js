//23+4

// cpu1.instructionDecoder
// ------------
cpu1_instructionDecoder = function(eng, path, name, delay, inertial, cp, t, ir, zero, carry,
    sig_mar_we, sig_dataio_re, sig_dataio_wr, memoryRequest, memoryRW, sig_ir_we, sig_ira_we, sig_pc_ce, sig_pc_sel,
    sig_mux_mar_sel, sig_regfile_addr_a, sig_regfile_addr_b, sig_regfile_addr_c, sig_regfile_we,
    sig_mux_regfile_c_sel, 
    sig_alu_cin, sig_alu_pass, sig_alu_rep, sig_alu_binv, sig_alu_bsel, sig_alu_rsel,
    sig_zeroff_we, sig_carryff_we, sig_cpDisable
) {
  let circName = path+"/"+name;

  let drv_mar_we = eng.createDriver(circName+"/sig_mar_we");
  let drv_dataio_re = eng.createDriver(circName+"/sig_dataio_re");
  let drv_dataio_wr = eng.createDriver(circName+"/sig_dataio_wr");
  let drv_memoryRequest = eng.createDriver(circName+"/memoryRequest");
  let drv_memoryRW = eng.createDriver(circName+"/memoryRW");
  let drv_ir_we = eng.createDriver(circName+"/sig_ir_we");
  let drv_ira_we = eng.createDriver(circName+"/sig_ira_we");
  let drv_pc_ce = eng.createDriver(circName+"/sig_pc_ce");
  let drv_pc_sel = eng.createDriver(circName+"/sig_pc_sel");
  let drv_mux_mar_sel_0 = eng.createDriver(circName+"/sig_mux_mar_sel_0");
  let drv_mux_mar_sel_1 = eng.createDriver(circName+"/sig_mux_mar_sel_1");
  let drv_regfile_addr_a_0 = eng.createDriver(circName+"/sig_regfile_addr_a_0");
  let drv_regfile_addr_a_1 = eng.createDriver(circName+"/sig_regfile_addr_a_1");
  let drv_regfile_addr_b_0 = eng.createDriver(circName+"/sig_regfile_addr_b_0");
  let drv_regfile_addr_b_1 = eng.createDriver(circName+"/sig_regfile_addr_b_1");
  let drv_regfile_addr_c_0 = eng.createDriver(circName+"/sig_regfile_addr_c_0");
  let drv_regfile_addr_c_1 = eng.createDriver(circName+"/sig_regfile_addr_c_1");
  let drv_regfile_we = eng.createDriver(circName+"/sig_regfile_we");
  let drv_mux_regfile_c_sel = eng.createDriver(circName+"/sig_mux_regfile_c_sel");
  let drv_alu_cin = eng.createDriver(circName+"/sig_alu_cin");
  let drv_alu_pass = eng.createDriver(circName+"/sig_alu_pass");
  let drv_alu_rep = eng.createDriver(circName+"/sig_alu_rep");
  let drv_alu_binv = eng.createDriver(circName+"/sig_alu_binv");
  let drv_alu_bsel = eng.createDriver(circName+"/sig_alu_bsel");
  let drv_alu_rsel = eng.createDriver(circName+"/sig_alu_rsel");
  let drv_zeroff_we = eng.createDriver(circName+"/sig_zeroff_we");
  let drv_carryff_we = eng.createDriver(circName+"/sig_carryff_we");
  let drv_cpDisable = eng.createDriver(circName+"/sig_cpDisable");

  cp.listeners.push(this);
  zero.listeners.push(this);
  carry.listeners.push(this);
  for(let i = 0; i < t.length; i++) t[i].listeners.push(this);
  for(let i = 0; i < ir.length; i++) ir[i].listeners.push(this);

  function instructionIndex() {
    let lvl1 = eng.binaryToIndex([ir[0],ir[1],ir[2],ir[3]]);
    let lvl2 = eng.binaryToIndex([ir[4],ir[5]]);
    if(lvl1.err || lvl2.err) return -1;
    if(lvl1.index >= 0 && lvl1.index <= 4) return lvl1.index;
    if(lvl1.index == 8 && lvl2.index >= 0 && lvl2.index <= 2) return lvl1.index*4+lvl2.index;
    if(lvl1.index >= 9 && lvl1.index <= 13) return lvl1.index;
    if(lvl1.index == 14 && lvl2.index >= 0 && lvl2.index <= 2) return lvl1.index*4+lvl2.index;
    if(lvl1.index == 15 && lvl2.index == 3 && ir[6].value==3 && ir[7].value==3) return 255;
    return -1;
  }

  let ONE = 3;
  let ZERO = 2;

  function bitValue(signalArray, index) {
    return signalArray[index].value == ONE;
  }
  function isin(v,vs) {
    return vs.indexOf(v) != -1;
  }
  this.execute = function() {
    let T1 = t[0].value === ONE;
    let T2 = t[1].value === ONE;
    let T3 = t[2].value === ONE;
    let T4 = t[3].value === ONE;
    let T5 = t[4].value === ONE;
    let T6 = t[5].value === ONE;
    let T7 = t[6].value === ONE;
    let II = instructionIndex();
    console.log("Decoded instruction index is " + II);
    let val_cpDisable = T3 && II == 255;
    let val_mar_we = T1 || (T3 && isin(II,[9,10,32,33,34,56,57,58])) || (T5 && isin(II,[33,34]));
    let val_dataio_re = (T2 && cp.value === ZERO) || (T4 && isin(II,[9,32,33,34,56,57,58]) && cp.value === ZERO) || (T6 && isin(II,[33]) && cp.value === ZERO);
    let val_dataio_wr = (T4 && isin(II,[10]) && cp.value === ZERO) || (T6 && isin(II,[34]) && cp.value === ZERO);
    let val_memoryRequest = (T2 && cp.value === ZERO) || (T4 && isin(II,[9,10,32,33,34,56,57,58]) && cp.value === ZERO) || (T6 && isin(II,[33,34]) && cp.value === ZERO);
    let val_memoryRW = T2 || (T4 && isin(II,[9,32,33,34,56,57,58])) || (T6 && isin(II,[33]));
    let val_ir_we = T2;
    let val_ira_we = (T4 && isin(II,[9,32,33,34,56,57,58])) || (T6 && isin(II,[33]));
    let val_pc_ce = T2 || (T4 && isin(II,[32,33,34,56,57,58])) || (T5 && ((isin(II,[56]))||(isin(II,[57]) && zero.value==ONE)||(isin(II,[58])&&carry.value==ONE)));
    let val_pc_sel = (T5 && ((isin(II,[56]))||(isin(II,[57]) && zero.value==ONE)||(isin(II,[58])&&carry.value==ONE)));
    let val_mux_mar_sel_0 = (T3 && isin(II,[9,10]));
    let val_mux_mar_sel_1 = (T5 && isin(II,[33,34]));
    let val_regfile_addr_a_0 = (T3 && isin(II,[1,2,3,12]) && bitValue(ir,4));
    let val_regfile_addr_a_1 = (T3 && isin(II,[1,2,3,12]) && bitValue(ir,5));
    let val_regfile_addr_b_0 = (T3 && ((isin(II,[0,1,2,3,4,9,10]) && bitValue(ir,6))||(isin(II,[11,13]) && bitValue(ir,4)))) || (T4 && isin(II,[10]) && bitValue(ir,4)) || (T6 && isin(II,[34]) && bitValue(ir,6));
    let val_regfile_addr_b_1 = (T3 && ((isin(II,[0,1,2,3,4,9,10]) && bitValue(ir,7))||(isin(II,[11,13]) && bitValue(ir,5)))) || (T4 && isin(II,[10]) && bitValue(ir,5)) || (T6 && isin(II,[34]) && bitValue(ir,7));
    let val_regfile_addr_c_0 = (T3 && isin(II,[0,1,2,3,4,11,12]) && bitValue(ir,4)) || (T5 && ((II == 32 && bitValue(ir,6)) || (II == 9 && bitValue(ir,4)))) || (T7 && II == 33 && bitValue(ir,6));
    let val_regfile_addr_c_1 = (T3 && isin(II,[0,1,2,3,4,11,12]) && bitValue(ir,5)) || (T5 && ((II == 32 && bitValue(ir,7)) || (II == 9 && bitValue(ir,5)))) || (T7 && II == 33 && bitValue(ir,7));
    let val_regfile_we = (T3 && isin(II,[0,1,2,3,4,11,12])) || (T5 && isin(II,[9,32])) || (T7 && isin(II,[33]));
    let val_mux_regfile_c_sel = (T5 && isin(II,[9,32])) || (T7 && isin(II,[33]));
    let val_alu_cin = (T3 && isin(II,[2,11]));
    let val_alu_pass = (T3 && isin(II,[1,12]));
    let val_alu_rep = (T3 && isin(II,[12]));
    let val_alu_binv = (T3 && isin(II,[2,4]));
    let val_alu_bsel = (T3 && isin(II,[0,1,2,4,9,10,11,13])) || (T4 && isin(II,[10])) || (T6 && isin(II,[34]));
    let val_alu_rsel = (T3 && isin(II,[3]));
    let val_zeroff_we = (T3 && isin(II,[1,2,3,4,11,12,13]));
    let val_carryff_we = (T3 && isin(II,[1,2,3,4,11,12]));

    eng.scheduleDriverTransaction(drv_mar_we.index, sig_mar_we.index, val_mar_we ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_dataio_re.index, sig_dataio_re.index, val_dataio_re ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_dataio_wr.index, sig_dataio_wr.index, val_dataio_wr ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_memoryRequest.index, memoryRequest.index, val_memoryRequest ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_memoryRW.index, memoryRW.index, val_memoryRW ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_ir_we.index, sig_ir_we.index, val_ir_we ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_ira_we.index, sig_ira_we.index, val_ira_we ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_pc_ce.index, sig_pc_ce.index, val_pc_ce ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_pc_sel.index, sig_pc_sel.index, val_pc_sel ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_mux_mar_sel_0.index, sig_mux_mar_sel[0].index, val_mux_mar_sel_0 ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_mux_mar_sel_1.index, sig_mux_mar_sel[1].index, val_mux_mar_sel_1 ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_regfile_addr_a_0.index, sig_regfile_addr_a[0].index, val_regfile_addr_a_0 ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_regfile_addr_a_1.index, sig_regfile_addr_a[1].index, val_regfile_addr_a_1 ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_regfile_addr_b_0.index, sig_regfile_addr_b[0].index, val_regfile_addr_b_0 ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_regfile_addr_b_1.index, sig_regfile_addr_b[1].index, val_regfile_addr_b_1 ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_regfile_addr_c_0.index, sig_regfile_addr_c[0].index, val_regfile_addr_c_0 ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_regfile_addr_c_1.index, sig_regfile_addr_c[1].index, val_regfile_addr_c_1 ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_regfile_we.index, sig_regfile_we.index, val_regfile_we ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_mux_regfile_c_sel.index, sig_mux_regfile_c_sel.index, val_mux_regfile_c_sel ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_alu_cin.index, sig_alu_cin.index, val_alu_cin ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_alu_pass.index, sig_alu_pass.index, val_alu_pass ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_alu_rep.index, sig_alu_rep.index, val_alu_rep ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_alu_binv.index, sig_alu_binv.index, val_alu_binv ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_alu_bsel.index, sig_alu_bsel.index, val_alu_bsel ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_alu_rsel.index, sig_alu_rsel.index, val_alu_rsel ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_zeroff_we.index, sig_zeroff_we.index, val_zeroff_we ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_carryff_we.index, sig_carryff_we.index, val_carryff_we ? ONE:ZERO, eng.currentTime+delay, inertial);
    eng.scheduleDriverTransaction(drv_cpDisable.index, sig_cpDisable.index, val_cpDisable ? ONE:ZERO, eng.currentTime+delay, inertial);
  }
}


// cpu1
// ------------
function cpu1(eng, path, name, delay, inertial, reset, cpIn, data, address, memoryRequest, memoryRW) {
  function createSignalVector(nameKernel, n, initValue) {
    let res = [];
    for(let i = 0; i < n; i++) {
      res.push(eng.createSignal(nameKernel+i, dlsimul.std_logic_1164.valueIndex(initValue)));
    }
    return res;
  }

  if(data.length<1) throw new dlsimul.engine.engine_error("cpu1: data must have at least one element.");
  if(address.length<1) throw new dlsimul.engine.engine_error("cpu1: address must have at least one element.");
  this.circuits = [];

  let sig_ONE = eng.createSignal(path+"/"+name+"/ONE", dlsimul.std_logic_1164.valueIndex('1'));
  let sig_ZERO = eng.createSignal(path+"/"+name+"/ZERO", dlsimul.std_logic_1164.valueIndex('0'));
  let cp = eng.createSignal(path+"/"+name+"/cp", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_cpPassQ = eng.createSignal(path+"/"+name+"/cpPassQ", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_cpDisable = eng.createSignal(path+"/"+name+"/cpDisable", dlsimul.std_logic_1164.valueIndex('U'));
  let cpPassGate = new eng.dffsynsrCirc(path+"/"+name, "cpPassGate", delay, inertial, cpIn, sig_ZERO, reset, null, sig_cpDisable, sig_cpPassQ, null);
  let cpGate = new eng.andCirc(path+"/"+name, "cpGate", delay, inertial, [cpIn,sig_cpPassQ], cp);

  let sig_mar_we = eng.createSignal(path+"/"+name+"/mar_we", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_mar_data = createSignalVector(path+"/"+name+"/mar_data_", address.length, 'U');
  let sig_data_in = createSignalVector(path+"/"+name+"/data_in_", data.length, 'U');
  let sig_dataio_re = eng.createSignal(path+"/"+name+"/dataio_re", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_dataio_wr = eng.createSignal(path+"/"+name+"/dataio_wr", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_alu_rez = createSignalVector(path+"/"+name+"/alu_rez_", data.length, 'U');
  let sig_alu_cout = eng.createSignal(path+"/"+name+"/alu_cout", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_alu_z = eng.createSignal(path+"/"+name+"/alu_z", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_alu_cin = eng.createSignal(path+"/"+name+"/alu_cin", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_alu_pass = eng.createSignal(path+"/"+name+"/alu_pass", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_alu_rep = eng.createSignal(path+"/"+name+"/alu_rep", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_alu_binv = eng.createSignal(path+"/"+name+"/alu_binv", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_alu_bsel = eng.createSignal(path+"/"+name+"/alu_bsel", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_alu_rsel = eng.createSignal(path+"/"+name+"/alu_rsel", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_ir_we = eng.createSignal(path+"/"+name+"/ir_we", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_ira_we = eng.createSignal(path+"/"+name+"/ira_we", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_ir_data = createSignalVector(path+"/"+name+"/ir_data_", data.length, 'U');
  let sig_ira_data = createSignalVector(path+"/"+name+"/ira_data_", data.length, 'U');
  let sig_pc_ce = eng.createSignal(path+"/"+name+"/pc_ce", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_pc_sel = eng.createSignal(path+"/"+name+"/pc_sel", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_pc = createSignalVector(path+"/"+name+"/pc_", data.length, 'U');
  let sig_zero_vector = createSignalVector(path+"/"+name+"/zero_", data.length, '0');
  let sig_mux_mar_sel = createSignalVector(path+"/"+name+"/mux_mar_sel_", 2, 'U');
  let sig_regfile_a = createSignalVector(path+"/"+name+"/regfile_a_", data.length, 'U');
  let sig_regfile_b = createSignalVector(path+"/"+name+"/regfile_b_", data.length, 'U');
  let sig_regfile_c = createSignalVector(path+"/"+name+"/regfile_c_", data.length, 'U');
  let sig_regfile_addr_a = createSignalVector(path+"/"+name+"/regfile_addr_a", 2, 'U');
  let sig_regfile_addr_b = createSignalVector(path+"/"+name+"/regfile_addr_b", 2, 'U');
  let sig_regfile_addr_c = createSignalVector(path+"/"+name+"/regfile_addr_c", 2, 'U');
  let sig_regfile_we = eng.createSignal(path+"/"+name+"/regfile_we", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_mux_regfile_c_sel = eng.createSignal(path+"/"+name+"/mux_regfile_c_sel", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_zero_flag = eng.createSignal(path+"/"+name+"/zero_flag", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_carry_flag = eng.createSignal(path+"/"+name+"/carry_flag", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_zeroff_we = eng.createSignal(path+"/"+name+"/zeroff_we", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_carryff_we = eng.createSignal(path+"/"+name+"/carryff_we", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_ringc_data = createSignalVector(path+"/"+name+"/ringc_data_", 7, 'U');

  // Control Unit generates:
  // sig_mar_we, sig_dataio_re, sig_dataio_wr, memoryRequest, memoryRW, sig_ir_we, sig_ira_we, sig_pc_ce, sig_pc_sel,
  // sig_mux_mar_sel[0:1], sig_regfile_addr_a[0:1], sig_regfile_addr_b[0:1], sig_regfile_addr_c[0:1], sig_regfile_we
  // sig_mux_regfile_c_sel, 
  // sig_alu_cin, sig_alu_pass, sig_alu_rep, sig_alu_binv, sig_alu_bsel, sig_alu_rsel
  // sig_zeroff_we, sig_carryff_we, sig_cpDisable

  let mar = new eng.nBitRegisterCirc(path+"/"+name, "mar", delay, inertial, cp, sig_mar_we, reset, sig_mar_data, address);
  let dataio = new eng.ioBufferCirc(path+"/"+name, "dataio", delay, inertial, data, sig_dataio_re, sig_dataio_wr, sig_data_in, sig_alu_rez, false, false);
  let ir = new eng.nBitRegisterCirc(path+"/"+name, "ir", delay, inertial, cp, sig_ir_we, reset, sig_data_in, sig_ir_data);
  let ira = new eng.nBitRegisterCirc(path+"/"+name, "ira", delay, inertial, cp, sig_ira_we, reset, sig_data_in, sig_ira_data);
  let pc = new eng.progCounter(path+"/"+name, "pc", delay, inertial, reset, cp, sig_pc_ce, sig_pc_sel, sig_ira_data, sig_pc);
  let mux_mar = new eng.mux_n_k_s(path+"/"+name, "mux_mar", delay, inertial, [sig_pc, sig_ira_data, sig_alu_rez, sig_zero_vector], sig_mux_mar_sel, sig_mar_data);
  let mux_regfile_c = new eng.mux_n_k_s(path+"/"+name, "mux_regfile_c", delay, inertial, [sig_alu_rez, sig_ira_data], [sig_mux_regfile_c_sel], sig_regfile_c);
  let regfile = new eng.registerBankCirc(path+"/"+name, "regfile", delay, inertial, sig_regfile_addr_a, sig_regfile_addr_b, sig_regfile_addr_c, sig_regfile_a, sig_regfile_b, sig_regfile_c, cp, sig_regfile_we, reset);
  let alu = new eng.alu1(path+"/"+name, "alu", delay, inertial, sig_regfile_a, sig_regfile_b, sig_alu_pass, sig_alu_rep, sig_alu_binv, sig_alu_bsel, sig_alu_cin, sig_alu_rsel, sig_alu_z, sig_alu_cout, sig_alu_rez);
  let zeroff_flag = new eng.dffsynsrCirc(path+"/"+name, "zeroff", delay, inertial, cp, sig_alu_z, null, reset, sig_zeroff_we, sig_zero_flag);
  let carryff_flag = new eng.dffsynsrCirc(path+"/"+name, "carryff", delay, inertial, cp, sig_alu_cout, null, reset, sig_carryff_we, sig_carry_flag);
  let ringc = new eng.nBitRingCounterCirc(path+"/"+name, "ringc", delay, inertial, cp, sig_ONE, reset, sig_ringc_data);
  let instrDecoder = new cpu1_instructionDecoder(eng, path+"/"+name, "instrdec", delay, inertial, cp, sig_ringc_data, sig_ir_data, sig_zero_flag, sig_carry_flag,
    sig_mar_we, sig_dataio_re, sig_dataio_wr, memoryRequest, memoryRW, sig_ir_we, sig_ira_we, sig_pc_ce, sig_pc_sel,
    sig_mux_mar_sel, sig_regfile_addr_a, sig_regfile_addr_b, sig_regfile_addr_c, sig_regfile_we,
    sig_mux_regfile_c_sel, 
    sig_alu_cin, sig_alu_pass, sig_alu_rep, sig_alu_binv, sig_alu_bsel, sig_alu_rsel,
    sig_zeroff_we, sig_carryff_we, sig_cpDisable
  );

  this.circuits.push(mar);
  this.circuits.push(dataio);
  this.circuits.push(ir);
  this.circuits.push(ira);
  this.circuits.push(pc);
  this.circuits.push(mux_mar);
  this.circuits.push(mux_regfile_c);
  this.circuits.push(regfile);
  this.circuits.push(alu);
  this.circuits.push(zeroff_flag);
  this.circuits.push(carryff_flag);
  this.circuits.push(ringc);
  this.circuits.push(instrDecoder);

  this.regfile = regfile;
  this.data = data;
  this.address = address;
  this.mr = memoryRequest;
  this.rw = memoryRW;
  this.cp = cp;
  this.reset = reset;
  this.pc_address = sig_pc;
  this.ir_data = sig_ir_data;
  this.ira_data = sig_ira_data;
  this.ringc_data = sig_ringc_data;
  this.zero_flag = sig_zero_flag;
  this.carry_flag = sig_carry_flag;
}

// cpu_system_1
// ------------
function cpu_system_1(eng, path, name, delay, inertial, reset, cp) {
  function createSignalVector(nameKernel, n, initValue) {
    let res = [];
    for(let i = 0; i < n; i++) {
      res.push(eng.createSignal(nameKernel+i, dlsimul.std_logic_1164.valueIndex(initValue)));
    }
    return res;
  }

  this.circuits = [];

  let AN = 8;
  let DN = 8;

  let sig_address = createSignalVector(path+"/"+name+"/address_", AN, 'U');
  let sig_data = createSignalVector(path+"/"+name+"/data_", DN, 'U');
  let sig_mr = eng.createSignal(path+"/"+name+"/mr", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_rw = eng.createSignal(path+"/"+name+"/rw", dlsimul.std_logic_1164.valueIndex('U'));
  let sig_cs1 = eng.createSignal(path+"/"+name+"/ONE", dlsimul.std_logic_1164.valueIndex('1'));

  let cpu = new cpu1(eng, path+"/"+name, "cpu", delay, inertial, reset, cp, sig_data, sig_address, sig_mr, sig_rw);
  let ram = new eng.ramCirc(path+"/"+name, "ram1", delay, inertial, sig_address, sig_rw, sig_mr, sig_cs1, sig_data);

  this.circuits.push(cpu);
  this.circuits.push(ram);

  this.address = sig_address;
  this.data = sig_data;
  this.rw = sig_rw;
  this.mr = sig_mr;
  this.cs1 = sig_cs1;
  this.cp = cp;
  this.reset = reset;
}

